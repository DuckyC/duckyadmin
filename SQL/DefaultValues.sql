-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             7.0.0.4332
-- --------------------------------------------------------


DROP TABLE IF EXISTS `bans`;
CREATE TABLE IF NOT EXISTS `bans` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text,
  `Reason` text,
  `SteamID` text,
  `UID` text,
  `UnbanTime` text,
  `BanTime` text,
  `BannedBy` text,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DELETE FROM `bans`;

DROP TABLE IF EXISTS `convars`;
CREATE TABLE IF NOT EXISTS `convars` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text,
  `Value` text,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;


DELETE FROM `convars`;
INSERT INTO `convars` (`ID`, `Name`, `Value`) VALUES
	(1, 'sv_allowcslua', '1'),
	(2, 'sv_kickerrornum', '0');

DROP TABLE IF EXISTS `ranks`;
CREATE TABLE IF NOT EXISTS `ranks` (
  `ID` int(10) DEFAULT NULL,
  `Name` text,
  `Color` text,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DELETE FROM `ranks`;
INSERT INTO `ranks` (`ID`, `Name`, `Color`) VALUES
	(10, 'Owner', '75;0;150'),
	(7, 'Admin', '0;200;0'),
	(8, 'Super Admin', '255;0;0'),
	(6, 'Moderator', '0;0;255'),
	(5, 'DJ', '255;255;255'),
	(4, 'VIP', '0;100;200'),
	(1, 'Player', '60;60;60'),
	(9, 'Developer', '0;0;0');

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` text,
  `Value` text,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

DELETE FROM `settings`;
INSERT INTO `settings` (`ID`, `Name`, `Value`) VALUES
	(1, 'DebugMode', '0'),
	(2, 'BanMessage', 'You have been banned from this server'),
	(3, 'KickMessage', 'You have been kicked from this server');

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Nickname` text,
  `UID` text NOT NULL,
  `SteamID` text NOT NULL,
  `Rank` text NOT NULL,
  `LastIP` text,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

DELETE FROM `users`;
INSERT INTO `users` (`ID`, `Nickname`, `UID`, `SteamID`, `Rank`, `LastIP`) VALUES
	(1, 'Ducky Canard', '2124625230', 'STEAM_0:1:33417065', '10', '192.168.1.76:27006');