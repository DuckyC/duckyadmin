DuckyAdmin.Ban = {}
DuckyAdmin.Ban.Bans = {}
local function addMinutes(x) return x * 60 end
local function addHours(x) return x * 60 * 60 end
local function addDays(x) return x * 60 * 60 * 24 end
local function addWeeks(x) return x * 60 * 60 * 24 * 7 end
local function addMonths(x) return x * 60 * 60 * 24 * 7 * 30 end

local function CheckBan(SteamID)
	local function Callback(Q, Data)
		if #Data > 0 then
			local UnbanTime = Data[1]["UnbanTime"]
			local SteamID = Data[1]["SteamID"]
			if os.time() > tonumber(UnbanTime) then
				DuckyAdmin.Ban.RemoveBanFromSQL(SteamID)
			else
				RunConsoleCommand("kickid", SteamID, "You are banned from this server")
			end			
		else
			print("Player not banned")
		end
	end
	DuckyAdmin.SDB.SQL.SendQuery(string.format("SELECT UnbanTime, SteamID FROM bans WHERE SteamID = %q", SteamID), Callback)
end
DuckyAdmin.Ban.CheckBan = CheckBan

local function UpdateBansOnServer()
	DuckyAdmin.SDB.SQL.SendQuery("SELECT * FROM bans", function(Q, Data) 
		PrintTable(Data)
	end)
end
DuckyAdmin.Ban.UpdateBansOnServer = UpdateBansOnServer
local function RemoveBanFromSQL(SteamID)
	DuckyAdmin.SDB.SQL.SendQuery(string.format("DELETE FROM bans WHERE SteamID = %q", SteamID), function() DuckyAdmin.Log("Removed ban on: %s", SteamID) hook.Call("DuckyAdmin_Unban", SteamID) end)
end
DuckyAdmin.Ban.RemoveBanFromSQL = RemoveBanFromSQL
local function BanPlayer(SteamID, Time, Reason, BannedBySteamID)
	PrintTable(Time)
	local Ply
	for k, v in pairs(player.GetAll()) do
		if v:SteamID() == SteamID then Ply = v end
	end
	local UnbanTime = os.time()
	local NiceTime = "You have been banned for "
	if Time["min"] then UnbanTime = UnbanTime + addMinutes(Time["min"]) NiceTime = string.format("%s %s %s", NiceTime, Time["min"], "Minutes") end
	if Time["hour"] then UnbanTime = UnbanTime + addHours(Time["hour"]) NiceTime = string.format("%s %s %s", NiceTime, Time["hour"], "Hours") end
	if Time["day"] then UnbanTime = UnbanTime + addDays(Time["day"]) NiceTime = string.format("%s %s %s", NiceTime, Time["day"], "Days") end
	if Time["week"] then UnbanTime = UnbanTime + addWeeks(Time["week"]) NiceTime = string.format("%s %s %s", NiceTime, "Weeks", Time["week"]) end
	if Time["month"] then UnbanTime = UnbanTime + addMonths(Time["month"]) NiceTime = string.format("%s %s %s", NiceTime, Time["month"], "Months") end
	if UnbanTime == os.time() then return end
	NiceTime = NiceTime..": "..Reason
	print(NiceTime)
	local QStr = [[INSERT INTO bans (Name, Reason, UID, SteamID, UnbanTime, BanTime, BannedBy) VALUES (%q, %q, %q, %q, %q, %q, %q)]]
	if IsValid(Ply) then
		DuckyAdmin.SDB.SQL.SendQuery(string.format(QStr, Ply:GetName(), Reason, Ply:UniqueID(), SteamID, UnbanTime, os.time(), BannedBySteamID), function() DuckyAdmin.Log("Added ban on: %s", SteamID) end)
		Ply:Kick(NiceTime)
	else
		DuckyAdmin.SDB.SQL.SendQuery(string.format(QStr, "Unknown", Reason, "Unknown", SteamID, UnbanTime, os.time(), BannedBySteamID), function() DuckyAdmin.Log("Added ban on: %s", SteamID) end)
	end
	hook.Call("DuckyAdmin_Ban", SteamID, Ply, Reason, UnbanTime, BannedBySteamID)
	return NiceTime
end
DuckyAdmin.Ban.BanPlayer = BanPlayer
print("Updated", os.time())
local validWords = {
minutes = "min",
hours = "hour",
days = "day",
weeks = "week",
months = "month",
years = "year",

min = "min",
h = "hour",
m = "min",
mon = "month",
w = "week",
y = "year",
}

local function Parse( Str )
	local Ret = {}
	for number, word in string.gmatch( Str, "(%d+) ?(%w+)" ) do
		if number and word and tonumber(number) and validWords[word] then
			Ret[validWords[word]] = tonumber(number)
		end
	end
	print("ret")
	PrintTable(Ret)
	return Ret
end
DuckyAdmin.Ban.Parse = Parse