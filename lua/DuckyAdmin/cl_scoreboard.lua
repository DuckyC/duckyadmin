local aowl = DuckyAdmin
if CLIENT then -- DAvatarImage
	local PANEL={}
	language.Add("NO_AVATAR",     "No Avatar")
	language.Add("NO_AVATAR_TINY","missing")
	function PANEL:Init()
		self.avatar=vgui.Create('AvatarImage',self)
		local overlay=vgui.Create('EditablePanel',self)
		
		function overlay.OnMousePressed(_,...)
			self:OnMousePressed(...)
		end
		function overlay.OnMouseReleased(_,...)
			self:OnMouseReleased(...)
		end
		function overlay.OnCursorEntered(_,...)
			self:OnCursorEntered(...)
		end
		function overlay.OnCursorExited(_,...)
			self:OnCursorExited(...)
		end
		function overlay.OnCursorMoved(_,...)
			self:OnCursorMoved(...)
		end
		function overlay.OnMouseWheeled(_,...)
			self:OnMouseWheeled(...)
		end
		
		self:SetSize(32,32)
		
		self.overlay=overlay
	end
	function PANEL:PerformLayout()
		self.avatar:SetSize(self:GetWide(),self:GetTall())
		self.overlay:SetSize(self:GetWide(),self:GetTall())
	end

	function PANEL:OnMousePressed()	end
	function PANEL:OnMouseReleased()	end
	function PANEL:OnCursorEntered()	end
	function PANEL:OnCursorExited()	end
	function PANEL:OnCursorMoved()	end
	function PANEL:OnMouseWheeled()	end

	function PANEL:UsingWeb()
		return self.__html
	end

	function PANEL:AssignImageURL(url)
		if !url or url:len()<4 then error("Invalid url",2) end
		if not self.__html then
			self.__html=true
			self.avatar:Remove()
			self.avatar=vgui.Create('HTML',self)
			self.avatar:SetSize(32,32)
			
			self.overlay:SetZPos(10)
			self.avatar:SetZPos(5)
			
			self.avatar.SetPlayer=function() error"nope" end
		end
		timer.Simple(0,function()
		self.avatar:SetHTML([[<html><head><style type="text/css">body {
	margin: 0px;
	padding: 0px;
	border: 0px;
	overflow: hidden;
	background-color: #000;
	-webkit-background-size: cover;
	background: url("]]..url..[[") no-repeat center center fixed;
	}</head><body></body></html>]]) end)
		self:InvalidateLayout()	
	end

	function PANEL:SetPlayer(pl,dimension)
		if self.player then return end
		self.player=pl
		dimension=dimension==32 and 32 or dimension==16 and 16 or dimension==64 and 64 or dimension==184 and 184 or 32
		
		self.Paint=(self.Paint and self.Paint!=self.Paint64) or dimension==184 and self.Paint184 or dimension==16 and self.Paint16 or self.Paint64
		
		self.dimension=dimension
		self.avatar:SetPlayer(pl,dimension)
		self.avatar:SetSize(dimension,dimension)
		self:SetSize(dimension,dimension)
		self:InvalidateLayout()
	end

	function PANEL:GetDimension()
		return self.dimension,self.dimension
	end

	function PANEL:GetPlayer()
		return self.player
	end

	local surface,draw,c,cr=surface,draw,Color(255,255,255,255),Color(255,100,100,255)
	function PANEL:Paint184()
		--surface.SetDrawColor(255,255,255,255)
		--surface.SetMaterial(nope)
		--surface.DrawTexturedRect(0,0,self:GetWide(),self:GetTall())
		surface.SetDrawColor(40,40,40,255)
		--surface.SetMaterial(nope)
		local t=self:GetTall()
		local w=self:GetWide()
		surface.DrawRect(0,0,w,t)
		draw.SimpleText("#NO_AVATAR", "Default", w*0.5, t*0.5, c, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	function PANEL:Paint64()
		--surface.SetDrawColor(255,255,255,255)
		--surface.SetMaterial(nope)
		--surface.DrawTexturedRect(0,0,self:GetWide(),self:GetTall())
		surface.SetDrawColor(40,40,40,255)
		--surface.SetMaterial(nope)
		local t=self:GetTall()
		local w=self:GetWide()
		surface.DrawRect(0,0,w,t)
		draw.SimpleText("#NO_AVATAR_TINY", "DefaultSmallDropShadow", w*0.5, t*0.5, c, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	function PANEL:Paint16()
		--surface.SetDrawColor(255,255,255,255)
		--surface.SetMaterial(nope)
		--surface.DrawTexturedRect(0,0,self:GetWide(),self:GetTall())
		surface.SetDrawColor(40,40,40,255)
		--surface.SetMaterial(nope)
		local t=self:GetTall()
		local w=self:GetWide()
		surface.DrawRect(0,0,w,t)
		draw.SimpleText("?", "DermaDefault", w*0.5, t*0.5, cr, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	PANEL.Paint=PANEL.Paint64

	function PANEL:GetOverlay()
		return self.overlay
	end

	derma.DefineControl("AOWLAvatarImage","Clickable avatar ",	PANEL,"EditablePanel")



end



if CLIENT then -- scoreboard
	local score = aowl.Score or {}

	do -- main

		-- Some colours that will be used.
		score.Colors = {
			white = Color(255, 255, 255, 255),
			white_trans = Color(255, 255, 255, 60),
			button = Color(220, 220, 220, 255),
			fore = Color(238, 238, 238, 255),
			forefake = Color(238-50, 238-50, 238-50, 255),
			fore_dimmed = Color(220, 220, 220, 255),
			back_trans = Color(51, 51, 51, 60),
			back = Color(51, 51, 51, 179),
			back_dimmed = Color(38, 38, 38, 255),
		}

		-- Font used by the scoreboard & buttons.
		surface.CreateFont("aowl_scoreboard_default", {size = 14, weight = 550, antialias = false, font = "DermaDefault"})
		surface.CreateFont("aowl_scoreboard_button", {size = 13, weight = 550, antialias = true, font = "DermaDefault"})
		surface.CreateFont("aowl_scoreboard_smaller", {size = 11, weight = 450, antialias = true, font = "DermaDefault"})

		score.help = "right click to show buttons / activate scrollbar"

		score.Info = {}
		function score.AddInfo(tbl) tbl.width = tbl.width / 100 table.insert(score.Info, tbl) end
		
		score.FakePlayers = {}
		
		function score.AddFake(name, uid, steamid, team, coins, id, ping, buttons)
			if not name or not uid then return end
			table.insert(score.FakePlayers, {
				name = name,
				uid = uid,
				steamid = steamid or 0,
				team = team or 1,
				coins = coins or 0,
				id = id or 0,
				ping = ping or 0,
				buttons = buttons or nil,
			})
			aowl.UpdateScoreboard(true)
		end
		
		function score.DeleteFake(uid)
			if not score.panel or (score.panel and not score.panel.Teams) then return end
			for k, v in pairs(score.FakePlayers) do
				if v.uid == uid then
					for team_id, data in pairs(score.panel.Teams) do
						if data.fakeplayers && data.fakeplayers[v.uid] then
							data.fakeplayers[v.uid]:Remove()
							data.fakeplayers[v.uid] = nil
						end
					end
					score.FakePlayers[k] = nil
				end
			end
		end

		local meta = FindMetaTable("Player")

		----- this should not be here, probably -----
		local function PingInfo(p)
			if type(p) == "table" then return p.ping or 0 end
			return (not p or not p.IsValid or not p:IsValid()) and "?"
				or (p:Ping()==-1 and "Timing out")
				or (p.IsAFK and p:IsAFK() and "AFK: "..p:Ping())
				or p:Ping()
		end
		---------------------------------------------

		score.AddInfo
		{
			width = 50,
			title = "",
			xoffset = 23,
			f = function(p)
					if type(p) == "table" then
						return p.name:gsub("%^%d", "")
					elseif type(p) == "Player" then
						if IsValid(p) then
							return p:GetName():gsub("%^%d", "")
						else
							return "" -- The player is going to be removed with next update, don't error on GetName kthx
						end
					end
				end,
		}


		score.AddInfo
		{
			width = 84,
			title = "id",
			f = function(p)
				if type(p) == "table" then
						return p.id or 0
					end
				return FindMetaTable("Entity").EntIndex(p)
			end,
		}

		score.AddInfo
		{
			width = 89,
			title = "ping",
			f = function(p)
				if type(p) == "table" then
						return p.ping or 0
					end
				return PingInfo(p)
			end,
		}

		score.Buttons =
		{
			{
				title = "goto",
				command = "da goto {NAME}",
				group = "players",
				show = "not_self",
			},
			{
				title = "bring",
				command = "da bring {NAME}",
				group = "developers",
				show = "not_self",

			},
			{
				title = "tp",
				command = "da tp",
				group = "players",
				show = "self",
			},
			{
				title = "spawn",
				command = "da spawn",
				group = "players",
				show = "self",
			},
			
			{
				title = "mute",
				command = "da mute {NAME}",
				group = "players",
				show = "not_self",
				update = function(btn, cmd, ply)
					if ply:IsMuted() then
						btn.Text = "unmute"
					else
						btn.Text = "mute"
					end
				end,
				press = function(btn, cmd, ply)
					ply:SetMuted(not ply:IsMuted())
				end,
			},
			{
				title = "kick",
				command = "da kick {NAME}",
				group = "developers",
				show = "not_self",
				press = function(btn, cmd, ply)
					LocalPlayer():ConCommand(cmd)
				end
			},
			{
				title = "ban",
				command = "da ban {NAME}",
				group = "developers",
				show = "not_self",
				update = function(btn, cmd, ply)
					if not ply.GetRestricted then return end
					if ply:GetRestricted() then
						btn.Text = "unban"
					else
						btn.Text = "ban"
					end
				end,
				press = function(btn, cmd, ply)
					if not ply.GetRestricted then return end
					if ply:GetRestricted() then
						LocalPlayer():ConCommand(cmd:gsub(" ban ", " unban "))
					else
						LocalPlayer():ConCommand(cmd)
					end
				end,
			},
			{
				title = "clean",
				command = "da cleanup {NAME}",
				group = "developers",
				show = "not_self",
			},
			{
				title = "spawn",
				command = "da spawn {NAME}",
				group = "developers",
				show = "not_self",
			},
		}

		if IsValid(score.panel) then
			score.panel:Remove()
		end
		
		score.panel = score.panel or NULL
		
		function aowl.RemoveScoreboardbutton(title)
			for key, value in pairs(score.Buttons) do
				if value.title == title then
					score.Buttons[key] = nil
					return key
				end
			end
		end

		function aowl.RemoveScoreboardbutton(title)
			for key, value in pairs(score.Info) do
				if value.title == title then
					score.Info[key] = nil
					return key
				end
			end
		end

		function aowl.AddScoreboardButton(data, pos)
			local last = aowl.RemoveScoreboardbutton(data.title)
			pos = pos or last or #score.Buttons

			local idx = table.insert(score.Buttons, pos, data)

			aowl.UpdateScoreboard()

			return idx
		end

		function aowl.AddScoreboardInfo(data, pos)
			local last = aowl.RemoveScoreboardbutton(data.title)
			pos = pos or #score.Buttons
			data.width = data.width / 100

			local idx = table.insert(score.Info, pos, data)

			aowl.UpdateScoreboard()

			return idx
		end

		function score.CreateScoreboard()
			if score.panel:IsValid() then
				score.panel:Remove()
			end

			score.panel = vgui.Create("aowl_scoreboard")
			score.panel:SetVisible(false)

			return true
		end
		hook.Add("CreateScoreboard", "aowl_scoreboard_CreateScoreboard", score.CreateScoreboard)

		local first = true

		function score.ScoreboardShow()
			if not score.panel:IsValid() then
				score.CreateScoreboard()
			end

			score.panel:SetVisible(true)

			local is_admin = LocalPlayer():IsAdmin()
			
			if first then
				aowl.UpdateScoreboard(true)
				first = false
			end

			score.visible = true

			return true
		end
		hook.Add("ScoreboardShow", "aowl_scoreboard_ScoreboardShow", score.ScoreboardShow)

		function score.ScoreboardHide()
			if not score.panel:IsValid() then
				score.CreateScoreboard()
			end

			score.panel:SetVisible(false)
			gui.EnableScreenClicker(false)

			score.visible = false

			return true
		end
		hook.Add("ScoreboardHide", "aowl_scoreboard_ScoreboardHide", score.ScoreboardHide)

		function score.PlayerBindPress(ply, bind, pressed)
			if not score.panel:IsValid() then
				return
			end

			if score.panel:IsVisible() and pressed and bind == "+attack2" then
				gui.EnableScreenClicker(true)
				return true
			end
		end
		hook.Add("PlayerBindPress", "aowl_scoreboard_PlayerBindPress", score.PlayerBindPress)
	end

	do -- aowl_scoreboard
		local PANEL = vgui.Register("aowl_scoreboard", {}, "DPanel")

		local P = 2
		local player_panel_height = 24

		function PANEL:Init()
			self.Teams = {}

			self:SetSize(650, ScrH()/1.2)
			self:Center()
			--self:SetDrawBackground(false)

			self:DockPadding(P,P,P,P)

			local pnl = vgui.Create("DLabel", self)
				pnl:SetFont("aowl_scoreboard_default")
			self.hostname = pnl

			local pnl = vgui.Create("DLabel", self)
				pnl:SetFont("aowl_scoreboard_button")
			self.help = pnl

			local pnl = vgui.Create("DPanelList", self)
				pnl:DockMargin(P, P*12, P, P*6)
				pnl:Dock(FILL)
				pnl:EnableVerticalScrollbar(true)
			self.list = pnl

			self:PerformLayout()
		end

		function PANEL:Paint()
			draw.RoundedBox(4, 0,0,self:GetWide(), self:GetTall(), score.Colors.back)
			surface.SetDrawColor(score.Colors.white_trans)
			surface.DrawLine(P*3,P*10,self:GetWide()-(P*3),P*10)
		end

		function PANEL:CreatePlayerPanel(ply, fake_buttons)
			local ply_panel = vgui.Create("DPanel")
				ply_panel:SetTall((player_panel_height + (P*2)))
				ply_panel:DockPadding(P,P,P,P)
				ply_panel.Paint = function(s)
					-- if not ply:IsValid() then return end

					surface.SetDrawColor(score.Colors.back)
					surface.DrawOutlinedRect(0,0,self:GetWide(), self:GetTall())

					local x = 0
					for idx, data in pairs(score.Info) do
						local x = self:GetWide() * data.width
						local pos = P+(data.xoffset or x) + P*2
						surface.SetTextPos(1+pos+P*2, P)
						surface.SetFont("aowl_scoreboard_default")
						surface.SetTextColor(score.Colors.back_dimmed)
						surface.DrawText(data.f(ply) or "ERR")
					end
				end
			local avatar = nil
			if type(ply) == "table" and ply.steamid then
				avatar = vgui.Create("HTML", ply_panel)
					local commid = aowl.SteamIDToCommunityID(ply.steamid)
					aowl.AvatarForSteamID(
						ply.steamid,
						function(url)
							if not url or not IsValid(avatar) then return end
							avatar:SetHTML('<html><head><style type="text/css">body { margin: 0px; }</style></head><body><a href="http://steamcommunity.com/profiles/' .. commid .. '"><img src="' .. url .. '" width="24" height="24" border="0"></a></body></html>')
							avatar:SetVerticalScrollbarEnabled(false)
						end
					)
					avatar:SetSize(24,24)
					avatar:Dock(LEFT)
			else
				avatar = vgui.Create("AOWLAvatarImage", ply_panel)
					avatar:SetSize(player_panel_height,player_panel_height)
					if type(ply) == "Player" then
						avatar:SetPlayer(ply,16)
					end
					avatar:SetSize(player_panel_height,player_panel_height)
					avatar:Dock(LEFT)
					function avatar.OnMouseReleased(avatar,mc)
						if mc!=108 then return end
						local a=DermaMenu()						
							a:AddOption("Copy SteamID",function() 
								SetClipboardText(ply:SteamID())
							end)
							a:AddOption("Copy Community ID",function() 
								SetClipboardText(ply:SteamID())
							end)
							a:AddOption("Copy Name",function() 
								SetClipboardText(ply:Name())
							end)
							a:AddOption("Open Profile URL",function() 
								local url="http://steamcommunity.com/profiles/" .. ply:SteamID64()
								gui.OpenURL(url)
							end)
							a:AddOption("Copy Profile URL",function() 
								SetClipboardText("http://steamcommunity.com/profiles/"..ply:SteamID64())
							end)
							
							
						a:Open()
					end
					
					local hover 
					function avatar.MouseStatus(_,entered)
						if entered then
							hover = vgui.Create("AOWLAvatarImage")
							if ValidPanel(g_AvatarHover) then g_AvatarHover:Remove() end
							g_AvatarHover=hover
			
							hover.OnMouseReleased=avatar.OnMouseReleased
							hover.Think=function() 
								if hover.thought then
									hover.thought=false
									hover.nextremove=false
								else
									if hover.nextremove then
										hover:Remove()
									end
									hover.nextremove=true
								end
							end
							local x,y=avatar:LocalToScreen( )
							x=x+avatar:GetWide()-184
							x=x<0 and 0 or x
							hover:SetPos(x,y+avatar:GetTall()*0.5-92)
							hover:SetPlayer(ply,184)
							avatar.hover=hover
							hover.parent=avatar
						else
							if ValidPanel(hover) then 
								hover:Remove() 
							end
						end
					end
					
					function avatar.Think(self)
						if ValidPanel(hover) then hover.thought = true end
						local x,y=avatar:CursorPos( )
						local out = x<0 or y<0 or x>self:GetWide() or y>self:GetTall()
						if out and self.__entered then
							self.__entered=false
							self:MouseStatus(false)
						elseif not out and not self.__entered then
							self.__entered=true
							
							self:MouseStatus(true)
						end
					end
				avatar:SetMouseInputEnabled(true)
			end

			local grid = vgui.Create("DPanelList", ply_panel)
				grid:EnableVerticalScrollbar(false)
				grid:SetDrawBackground(false)
				grid:EnableHorizontal(true)
				grid:SetSize(1000, P*8)
				grid:AlignBottom(-3)
				grid:AlignLeft(avatar:GetWide() + P*4)
				grid:SetSpacing(P)
				grid.Paint = function() end

			for idx, data in pairs(score.Buttons) do
				if
					LocalPlayer():CheckUserGroupLevel(data.group) and
					(
						(ply == LocalPlayer() and data.show == "self") or
						(ply ~= LocalPlayer() and data.show == "not_self")
					)
				then
					if fake_buttons and not fake_buttons[data.title] then continue end
					local btn = vgui.Create("DPanel")
					btn.Text = data.title
					btn:SetHeight(P*5.5)

					surface.SetFont("aowl_scoreboard_smaller")
					local w,h = surface.GetTextSize(data.title)
					btn:SetWidth(w + (P*4))

					btn:SetCursor("hand")
					btn.Paint = function(s)
						if not vgui.CursorVisible() then return end
				

						surface.SetDrawColor(score.Colors.button)
						--surface.SetDrawColor( HSVToColor((string.byte(s.Text,1)*64+string.byte(s.Text,2)*22345)%360,btn.highlight and 0.4 or 0.2,0.9) )
						surface.DrawRect(0,0,s:GetWide(),s:GetTall())
						surface.SetDrawColor(score.Colors.back_trans)
						surface.DrawOutlinedRect(0,0,s:GetWide(),s:GetTall())

						if data.update then
							if(IsValid(ply) and ply:IsPlayer()) then
								data.update(btn, cmd, ply)
							end
						end

						surface.SetFont("aowl_scoreboard_smaller")

						if btn.LastText ~= btn.Text then
							w,h = surface.GetTextSize(btn.Text)
							btn:SetWidth(w + (P*4))
							grid:Rebuild()
							btn.LastText = btn.Text
						end

						surface.SetTextPos((s:GetWide()*0.5) - (w*0.5),(s:GetTall()*0.5) - (h*0.5))
						surface.SetTextColor(s.highlight and score.Colors.back_dimmed or score.Colors.back)
						surface.DrawText(btn.Text)

						return false
					end
					btn.OnCursorEntered = function()
						btn.highlight = true
					end

					btn.OnCursorExited = function()
						btn.highlight = false
					end

					local cmd = data.command:gsub("{NAME}", ply:GetName())					

					btn.OnMousePressed = function(btn,mc)
						if mc!=MOUSE_LEFT then return end
						-- if type(ply) ~= "Player" then return end
						if fake_buttons then
							if fake_buttons[data.title].OnClick then
								fake_buttons[data.title].OnClick(btn, cmd, ply)
							end
							return
						end
						if(IsValid(ply) and ply:IsPlayer()) then
							if data.press then
								data.press(btn, cmd, ply)
							else
								LocalPlayer():ConCommand(cmd)
							end
						end

					end
					grid:AddItem(btn)
				end
			end

			grid:Rebuild()

			return ply_panel
		end
		
		function PANEL:SetFakePlayer(ply, team_name, team_color, buttons)
			local uid = team_name .. team_color.r .. team_color.g .. team_color.b
			
			if self.Teams[uid] and self.Teams[uid].fakeplayers[ply.uid] then return end
			
			local ply_panel = self:CreatePlayerPanel(ply, buttons)
			
			if self.Teams[uid] then
				local list = self.Teams[uid].panel
				list:AddItem(ply_panel)

				self.Teams[uid].fakeplayers[ply.uid] = ply_panel
			else
				local collapse = vgui.Create("DCollapsibleCategory", self)
					collapse.Header:SetTall(P*9)
					collapse:SetPadding(P)
					collapse:SetExpanded(true)
					collapse.Header:SetFont("HudHintTextSmall")
					collapse:SetLabel("")
					collapse:SetAnimTime(0.05)
					collapse.Paint = function(s)

						draw.RoundedBox(4, 0,0,self:GetWide(), self:GetTall(), team_color)

						surface.SetTextPos(P*3, P)
						surface.SetFont("aowl_scoreboard_default")
						surface.SetTextColor(score.Colors.fore)
						surface.DrawText(team_name)

						local x = 0
						for idx, data in pairs(score.Info) do
							local x = self:GetWide() * data.width
							x = x + 2
							local pos = P+(data.xoffset or x) + P*2
							surface.SetTextPos(x+P*5, P)
							surface.SetFont("aowl_scoreboard_default")
							surface.SetTextColor(score.Colors.fore)
							surface.DrawText(data.title)

							if not data.xoffset then
								surface.SetDrawColor(score.Colors.back)
								surface.DrawLine((data.xoffset or x) + P*2, 0, (data.xoffset or x) + P*2, self:GetTall()+1)
							end
						end
					end

					local panel = vgui.Create("DPanelList", collapse)
						panel:AddItem(ply_panel)
						panel:SetAutoSize(true)
						panel.Paint = function(s)
						
							surface.SetDrawColor(score.Colors.fore)

							surface.DrawRect(0,0,self:GetWide(), self:GetTall())

							local x = 0
							for idx, data in pairs(score.Info) do
								local x = self:GetWide() * data.width
								surface.SetDrawColor(score.Colors.back)
								surface.DrawLine((data.xoffset or x) + P*2, 0, (data.xoffset or x) + P*2, self:GetTall()+1)
							end
						end

					
					collapse:SetContents(panel)
					collapse:PerformLayout()

				self.list:AddItem(collapse)

				self.Teams[uid] =
				{
					collapse = collapse,
					panel = panel,
					name = team_name,
					color = team_color,
					fakeplayers =
					{
						[ply.uid] = ply_panel,
					},
					players =
					{
					},
				}
			end
		end

		function PANEL:SetPlayer(ply, team_name, team_color)
			local uid = team_name .. team_color.r .. team_color.g .. team_color.b

			if self.Teams[uid] and self.Teams[uid].players[ply] then return end
			self:RemovePlayer(ply)
			local ply_panel = self:CreatePlayerPanel(ply)
			
			if self.Teams[uid] then
				local list = self.Teams[uid].panel
				list:AddItem(ply_panel)

				self.Teams[uid].players[ply] = ply_panel
			else
				local collapse = vgui.Create("DCollapsibleCategory", self)
					collapse.Header:SetTall(P*9)
					collapse:SetPadding(P)
					collapse:SetExpanded(true)
					collapse.Header:SetFont("HudHintTextSmall")
					collapse:SetLabel("")
					collapse:SetAnimTime(0.05)
					collapse.Paint = function(s)

						draw.RoundedBox(4, 0,0,self:GetWide(), self:GetTall(), team_color)

						surface.SetTextPos(P*3, P)
						surface.SetFont("aowl_scoreboard_default")
						surface.SetTextColor(score.Colors.fore)
						surface.DrawText(team_name)

						local x = 0
						for idx, data in pairs(score.Info) do
							local x = self:GetWide() * data.width
							x = x + 2
							local pos = P+(data.xoffset or x) + P*2
							surface.SetTextPos(x+P*5, P)
							surface.SetFont("aowl_scoreboard_default")
							surface.SetTextColor(score.Colors.fore)
							surface.DrawText(data.title)

							if not data.xoffset then
								surface.SetDrawColor(score.Colors.back)
								surface.DrawLine((data.xoffset or x) + P*2, 0, (data.xoffset or x) + P*2, self:GetTall()+1)
							end
						end
					end

					local panel = vgui.Create("DPanelList", collapse)
						panel:AddItem(ply_panel)
						panel:SetAutoSize(true)
						panel.Paint = function(s)
							surface.SetDrawColor(score.Colors.fore)
							surface.DrawRect(0,0,self:GetWide(), self:GetTall())

							local x = 0
							for idx, data in pairs(score.Info) do
								local x = self:GetWide() * data.width
								surface.SetDrawColor(score.Colors.back)
								surface.DrawLine((data.xoffset or x) + P*2, 0, (data.xoffset or x) + P*2, self:GetTall()+1)
							end
						end

					collapse:SetContents(panel)
					collapse:PerformLayout()

				self.list:AddItem(collapse)

				self.Teams[uid] =
				{
					collapse = collapse,
					panel = panel,
					name = team_name,
					color = team_color,
					players =
					{
						[ply] = ply_panel,
					},
					fakeplayers = 
					{
					},
				}
			end
		end

		function PANEL:Refresh()
			self:CheckPlayers()
			for team_id, data in pairs(self.Teams) do
				data.collapse:SetAnimTime(0)
				data.collapse:Toggle()
				data.collapse:Toggle()
				data.collapse:SetAnimTime(0.05)
			end
			self:PerformLayout()
		end
		
		function PANEL:DeleteFake(uid)
			for team_id, data in pairs(self.Teams) do
				for ply in pairs(data.fakeplayers) do
					if ply == uid then
						data.fakeplayers[uid]:Remove()
						data.fakeplayers[uid] = nil
					end
				end
			end
		end

		function PANEL:RemovePlayer(ply)
			if not IsValid(ply) then return end

			for team_id, data in pairs(self.Teams) do
				for ply in pairs(data.players) do
					if not ply:IsValid() then
						data.players[ply]:Remove()
						data.players[ply] = nil
					end
				end
				if data.players[ply] then
					data.players[ply]:Remove()
					data.players[ply] = nil
				end
				if table.Count(data.players) == 0 then
					self.Teams[team_id] = nil
					data.collapse:Remove()
				end
			end

			self:Refresh()
		end

		function PANEL:CheckPlayers()
			for team_id, data in pairs(self.Teams) do
				for ply in pairs(data.players) do
					if not ply:IsValid() then
						data.players[ply]:Remove()
						data.players[ply] = nil
					end
				end
				for ply in pairs(data.fakeplayers) do
					local yess = false
					for k, v in pairs(score.FakePlayers) do
						if v.uid == ply then
							yess = true
						end
					end
					if not yess then
						data.fakeplayers[ply]:Remove()
						data.fakeplayers[ply] = nil
					end
				end
				if table.Count(data.players) == 0 then
					self.Teams[team_id] = nil
					data.collapse:Remove()
				end
			end
		end

		function PANEL:PerformLayout()
			self.hostname:SetText(GetHostName())
			self.hostname:SizeToContents()
			self.hostname:Center()
			self.hostname:AlignTop(P)
			self.hostname:SetColor(Color(255, 255, 255))

			self.help:SetText(score.help)
			self.help:SizeToContents()
			self.help:Center()
			self.help:AlignBottom(P)
			self.help:SetColor(Color(255, 255, 255))

			for team_id, data in pairs(self.Teams) do
				data.panel:Rebuild()
				data.collapse:PerformLayout()
			end
		end

		local last_height

		function PANEL:Think()
			if vgui.CursorVisible() then
				self.help:SetVisible(false)
			else
				self.help:SetVisible(true)
			end

			local height = 0

			for team_id, data in pairs(self.Teams) do
				height = height + data.collapse:GetTall()
			end

			if height ~= last_height then
				if height > ScrH() / 1.2 then
					self:SetTall(ScrH() / 1.2)
				else
					self:SetTall(height + (P*20))
				end
				self:Center()
				self:PerformLayout()

				last_height = height
			end
		end

	end

	function aowl.UpdateScoreboard(force)
		print("update")
		if aowl.Score.panel:IsValid() and (aowl.Score.visible or force) then
			aowl.Score.panel:CheckPlayers()
			
			for key, ply in pairs(player.GetAll()) do
				local name = team.GetName(ply:Team())
				local color = team.GetColor(ply:Team())
				
				print("Player:", ply, "Name:", name, "Color:", color, "Team:", ply:Team())
				score.panel:SetPlayer(ply, name, color)--ply:GetAowlTeamName(), ply:GetAowlTeamColor())--
			end
			for k, v in pairs(score.FakePlayers) do
				local name = team.GetName(v.team)
				local color = team.GetColor(v.team)
			
				score.panel:SetFakePlayer(v, name, color, v.buttons)
			end
			score.panel:Refresh()
		end
	end

	local players = {}

	hook.Add("Think", "aowl_scoreboard_think", function()
	    if aowl.Score.visible then
			for key, ply in pairs(player.GetAll()) do
				local uid = ply:UniqueID()
				if not players[uid] then
					aowl.UpdateScoreboard()
				end
				players[uid] = ply
			end

			for uid, ply in pairs(players) do
				if not ply:IsValid() then
					players[uid] = nil
					aowl.UpdateScoreboard()
				end
			end
	    end
	end)

	usermessage.Hook("aowl_join_team", function(umr)
		local ply = umr:ReadEntity()

		if ply:IsValid() then
			aowl.UpdateScoreboard()
		end
	end)
	aowl.Score = score

	if LocalPlayer():IsValid() then
		if IsValid(aowl.Score.panel) then
			aowl.Score.panel:Remove()
			aowl.Score.panel = NULL
		end
	end
end
