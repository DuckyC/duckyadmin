local _R = debug.getregistry()
local Player = _R.Player

function Player:GetITable()
	if IsValid(self) then
		local Tbl = DuckyAdmin.SDB.Players[self:SteamID()]
		if Tbl then return Tbl
		else
			Player:UpdatePlayerOnServer()
			return {Rank = 1, Nickname = self:Nick()}
		end
	end
end

function Player:GetRank()
	return tonumber(self:GetITable().Rank)
end

function Player:GetRankColor()
	return DuckyAdmin.SDB.Ranks[self:GetRank()].Color
end

function Player:GetRankName()
	if not (self:GetRank() == 1) then return DuckyAdmin.SDB.Ranks[self:GetRank()].Name else return "" end 
end

function Player:GetName()
	return self:GetITable().Nickname 
end

function Player:CreatePlayerOnSQL()
	if IsValid(self) then
		DuckyAdmin.SDB.SQL.SendQuery(string.format("INSERT INTO users (Nickname, UID, SteamID, Rank, LastIP) VALUES (%q, %q, %q, %q, %q)", self:GetName(), self:UniqueID(), self:SteamID(), 1, self:IPAddress()), function() DuckyAdmin.Log("Created User: %s", self:GetName()) end)
	end
	DuckyAdmin.SendPlayers()
end
function Player:UpdatePlayerOnSQL()
	if IsValid(self)then
		DuckyAdmin.SDB.SQL.SendQuery(string.format("UPDATE users Set Nickname=%q, Rank=%q, LastIP=%q WHERE SteamID=%q", self:GetName(), self:GetRank(), self:IPAddress(), self:SteamID()), function() DuckyAdmin.Log("Updated User: %s", self:GetName()) end)
	end
	DuckyAdmin.SendPlayers()
end
function Player:UpdatePlayerOnServer()
	if IsValid(self) then
		DuckyAdmin.SDB.SQL.LoadData(self:SteamID())
	end
	DuckyAdmin.SendPlayers()
end

function Player:IsAdmin()
	if IsValid(self)then
		return self:GetRank() >= 8
	end
end

function Player:IsSuperAdmin()
	if IsValid(self) then
		return self:GetRank() >= 9 
	end
end
function Player:NiceName()
	if self:GetRank() > 1 then 
		return self:GetRankColor(), "["..self:GetRankName().."]", Color(60, 60, 60), self:GetName()
	else
		return Color(60, 60, 60), "", Color(60, 60, 60), self:GetName()
	end
end

function Player:CheckUserGroupLevel(name)
	return self:GetRank()
end

function Player:ShouldHideAdmins()
	return false
end

function Player:IsUserGroup(name)
	local PlyRank = self:GetRankName():lower()
	local CheckRank = name:lower()
	
	return PlyRank == CheckRank
end
function Player:GetUserGroup()
	return self:GetRankName():lower()
end