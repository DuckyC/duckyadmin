assert(type(DuckyAdmin)=="table")
DuckyAdmin.Utils = {}
DuckyAdmin.Chat = {}
DuckyAdmin.SDB = {}
DuckyAdmin.SDB.Ranks = {}
DuckyAdmin.SDB.Players = {}
DuckyAdmin.SDB.SQL = {}
DuckyAdmin.LogIt = true
DuckyAdmin.Console = {}
DuckyAdmin.SDB.NewUsers = {}
DuckyAdmin.SDB.Settings = {}

for k,v in pairs(file.Find("materials/icons16/*", "GAME")) do
    resource.AddFile("materials/icon16/" .. v)
end

util.AddNetworkString( "DuckyAdmin_FillDB" )
util.AddNetworkString( "DuckyAdmin_ChatText" )
util.AddNetworkString( "DuckyAdmin_RunLua" )
util.AddNetworkString( "DuckyAdmin_SendDB" )
util.AddNetworkString( "DuckyAdmin_UpdatePlayers" )
util.AddNetworkString( "DuckyAdmin_RequestSQL" )
util.AddNetworkString( "DuckyAdmin_SendSQL" )
util.AddNetworkString( "DuckyAdmin_DoSQL" )
util.AddNetworkString( "DuckyAdmin_DoSQLRecall" )
util.AddNetworkString( "DuckyAdmin_OpenSQLClient" )
util.AddNetworkString( "DuckyAdmin_UpdatePlayerOnSQL" )
util.AddNetworkString( "DuckyAdmin_UpdatePlayerOnServer" )



for _, FileName in pairs(file.Find("DuckyAdmin/cl_*.lua", "LUA")) do
	AddCSLuaFile("DuckyAdmin/"..FileName)
end
for _, FileName in pairs(file.Find("DuckyAdmin/sh_*.lua", "LUA")) do
	AddCSLuaFile("DuckyAdmin/"..FileName)
	include("DuckyAdmin/"..FileName)
end


include("DuckyAdmin/sv_sqlLib.lua")
DuckyAdmin.SDB.SQL.Init()
include("DuckyAdmin/sv_loadDB.lua")
include("DuckyAdmin/sv_player.lua")
include("DuckyAdmin/sv_hooks.lua")
include("DuckyAdmin/sv_bans.lua")
include("DuckyAdmin/sv_pluginHandler.lua")
