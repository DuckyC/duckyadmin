assert(SERVER)
require( "mysqloo" )

local DB
DuckyAdmin.SDB.SQL.Init = function()
	include("DuckyAdmin/sv_sqlSettings.lua")
	local Info = DuckyAdmin.SDB.SQL.Info
	DuckyAdmin.SDB.SQL.DBObj = mysqloo.connect(Info.Host, Info.User, Info.Pass, Info.Database, Info.Port)
	DB = DuckyAdmin.SDB.SQL.DBObj
	function DB:onConnected()
		DuckyAdmin.Log("DuckyAdmin: SQL Database -> Connected")
		DuckyAdmin.Log(self:hostInfo())
	end
	
	function DB:onConnectionFailed( err )
		DuckyAdmin.Log("DuckyAdmin: SQL Database CRITICAL ERROR: "..err)
		error("SQL Database: "..err)
	end
	Info = nil
	DuckyAdmin.SDB.SQL.Info = nil
	DB:connect()
	DuckyAdmin.Log("DuckyAdmin: SQL Database Connecting....")
	DB:wait()
end

local function SendQuery(QS, OnSuccess, OnData, OnError)
	if QS and (OnSuccess or OnData) then
		if OnSucccess then if !(type(OnSuccess) == "function") then DuckyAdmin.Log("Query[%s] OnSuccess not function!!!!", QS) return false end end
		if OnData then if !(type(OnData) == "function") then DuckyAdmin.Log("Query[%s] OnData not function!!!!", QS) return false end end
		if not DB then DuckyAdmin.SDB.SQL.Init() end
		local Query = DB:query( QS )
		if not OnError then OnError = function(q, err, sqline) DuckyAdmin.Log("Query[ %s ] Error: %s", sqline, err) end end
		if OnSuccess then Query.onSuccess = OnSuccess end
		if OnData then Query.onData = OnData end
		Query.onError = OnError
		Query:start()
		return {true, Query}
	else
		return {false, "No Querry or no callback"}
	end
	
end
DuckyAdmin.SDB.SQL.SendQuery = SendQuery

local function LoadData(SteamID, Data)
	local function Callback( Q, NewData )
		if #NewData > 0 then
			DuckyAdmin.SDB.Players[NewData[1]["SteamID"]] = table.Copy(NewData[1])
			DuckyAdmin.SDB.Players[NewData[1]["SteamID"]].UpdatedNick = false
			DuckyAdmin.Log("Loaded user: %s", NewData[1]["Nickname"])
			local Ply = DuckyAdmin.Utils.FindBySteamID(NewData[1]["SteamID"])
			Ply:SetTeam(Ply:GetRank())
		else
			DuckyAdmin.SDB.Players[SteamID] = DuckyAdmin.Utils.CreateFakePlayer(Data)
			DuckyAdmin.SDB.NewUsers[SteamID] = Q 
		end
		DuckyAdmin.SendPlayers()
	end
	DuckyAdmin.SDB.SQL.SendQuery(string.format("SELECT * FROM users WHERE SteamID = %q", SteamID), Callback)
end
DuckyAdmin.SDB.SQL.LoadData = LoadData
print("updated", os.time())

local function SendPlayers()
	net.Start("DuckyAdmin_UpdatePlayers")
		net.WriteTable(DuckyAdmin.SDB.Players)
	net.Broadcast()
	print("Send Players to everyone!")
end
DuckyAdmin.SendPlayers = SendPlayers