local _R = debug.getregistry()
local Player = _R.Player

function Player:GetITable()
	if IsValid(self) then
		local Tbl = DuckyAdmin.CDB.Players[self:SteamID()]
		if Tbl then return Tbl
		else
			return {}
		end
	end
end

function Player:GetRank()
	if IsValid(self) then
		local ITable = self:GetITable()
		if ITable then
			return tonumber(ITable.Rank)
		else
			return 1
		end
	end
end

function Player:GetRankColor()
	if IsValid(self) then
		return DuckyAdmin.CDB.Ranks[self:GetRank()].Color
	end
end

function Player:GetRankName()
	if IsValid(self) then
		if not (self:GetRank() == 1) then return DuckyAdmin.CDB.Ranks[self:GetRank()].Name else return "" end 
	end
end

function Player:GetName()
	return self:GetITable().Nickname 
end

function Player:IsAdmin()
	if IsValid(self) then
		local Rank = self:GetRank()
		return  Rank>= 8
	end
end

function Player:IsSuperAdmin()
	if IsValid(self) then
		return self:GetRank() >= 9 
	end
end

function Player:NiceName()
	if self:GetRank() > 1 then 
		return self:GetRankColor(), "["..self:GetRankName().."]", Color(60, 60, 60), self:GetName()
	else
		return Color(60, 60, 60), "", Color(60, 60, 60), self:GetName()
	end
end

function Player:CheckUserGroupLevel(name)
	return self:GetRank()
end

function Player:ShouldHideAdmins()
	return false
end

function Player:IsUserGroup(name)
	local PlyRank = self:GetRankName():lower()
	local CheckRank = name:lower()
	
	return PlyRank == CheckRank
end
function Player:GetUserGroup()
	return self:GetRankName():lower()
end