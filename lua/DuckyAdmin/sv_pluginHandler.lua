DuckyAdmin.PH = {}
DuckyAdmin.PH.Plugins = {}
DuckyAdmin.PH.Clientcode = {}

DuckyAdmin.PH.SubmitPlugin = function( Table )
	if DuckyAdmin.PH.Plugins[Table.Name] then DuckyAdmin.Log("Updated plugin: %s", Table.Name) end
	 DuckyAdmin.PH.Plugins[Table.Name] = table.Copy(Table)
	if Table.Hooks then
		 for Hookname, Function in pairs(Table.Hooks) do
			hook.Add(Hookname, "DuckyAdmin_" .. Table.Name, Function)
		 end
	end
	 if Table.ClientCode then
		DuckyAdmin.PH.ClientCode[Table.Name] = Table.ClientCode
	 end
end

DuckyAdmin.PH.TryCommand = function(Ply, Splode, Prefix, Command, Args, IsConsole)
	if !IsValid(Ply) then Ply["CheckRank"] = function() return 10 end end
	local Silent = false
	--PrintTable({Ply, Splode, Prefix, Command, Args, IsConsole})
	local AvailPlugins = {}
	for _, Plugin in pairs(DuckyAdmin.PH.Plugins) do
		if Plugin.Prefix == Prefix then table.insert(AvailPlugins, Plugin) end
		if Plugin.SilentPrefix == Prefix then
			table.insert(AvailPlugins, Plugin)
			Silent = true
		end
	end
	if #AvailPlugins > 1 then 
		for _, Plugin in pairs(AvailPlugins) do
			if string.lower(Plugin.Command) == string.lower(Command) then
				local Targets = {}
				local CanExecute = false
				if Plugin.NeedTarget then
					Targets = DuckyAdmin.PH.GetPlayers(Splode[2])
					if #Targets == 0 then Targets = {Ply} end
					table.remove(Args, 1)
					
				else
					Targets = {Ply}
				end
				CanExecute, Targets = CanTarget(Plugin, Ply, Targets)
				print(CanExecute)
				PrintTable(Targets)
				if CanExecute then
					local Rtn = {pcall(Plugin.RunFunction, Ply, Args, Targets, Silent)}
					if not Rtn[1] then DuckyAdmin.Log(Rtn[2])  end 
					return true
				else
					DuckyAdmin.Message(true, Ply, Color(255, 255, 255), "Access denied!")
				end
			end
		end
	elseif #AvailPlugins == 1 then
		local Plugin = AvailPlugins[1]
		local Targets = {}
		local CanExecute = false
		if Plugin.NeedTarget then
			Targets = DuckyAdmin.PH.GetPlayers(Splode[2])
			if #Targets == 0 then Targets = {Ply} end
			table.remove(Args, 1)
				
		else
			Targets = {Ply}
		end
		CanExecute, Targets = CanTarget(Plugin, Ply, Targets)
		if CanExecute then
			local Rtn = {pcall(Plugin.RunFunction, Ply, Args, Targets, Silent)}
			if not Rtn[1] then DuckyAdmin.Log(Rtn[2])  end 
			return true
		else
			DuckyAdmin.Message(true, Ply, Color(255, 255, 255), "Access denied!")
		end
	end
end

local function sanitizeregex( str )
	return string.gsub( str, "[%^%$%(%)%%%.%[%]%+%-%?]", "%%%1" )
end

DuckyAdmin.PH.GetPlayers = function(name)
	if not name or name == "" then return {} end
	if string.Left(name, 6) == "STEAM_" then return {name} end
	local regex = string.lower( string.gsub( sanitizeregex( name ), "*", ".-" ) )

	local targets = {}
	for k,v in pairs( player.GetAll() ) do
		if string.find( string.lower( v:GetName() ), regex )then
			targets[#targets+1] = v
		end
	end

	return targets
end

local function RunLua(String, Ply)
	net.Start("DuckyAdmin_RunLua")
	net.WriteString(String)
	if Ply then net.Send(Ply) else net.Broadcast() end
end
DuckyAdmin.RunLua = RunLua


DuckyAdmin.Message = function( ... )
	local tbl = {...}
	local targets
	if type(tbl[2]) == "Player" && tbl[1] == true then
		table.remove(tbl, 1)
		targets = table.remove( tbl, 1 )
	elseif type(tbl[2]) == "table" then
		local isOK = true
		for k,v in pairs( tbl[2] ) do
			if type(v) ~= "Player" then
				isOK = false
				break
			end
		end
		if isOK then
			targets = table.remove( tbl, 2 )
		end
	end

	for k1, v1 in pairs(tbl) do
		if type(v1) == "Player" then
			table.remove(tbl, k1)
			if IsValid(v1) then
				for k2, v2 in pairs({v1:NiceName()}) do
					table.insert(tbl, k1+k2-1, v2)
				end
			else
				table.insert(tbl, k1, DuckyAdmin.SDB.Ranks["10"].Color)
				table.insert(tbl, k1+1, DuckyAdmin.SDB.Ranks["10"].Name)
				table.insert(tbl, k1+2, "Console")
			end
		end
	end
	
	table.insert(tbl, 1, Color(255, 150, 0))
	table.insert(tbl, 2, "[DA]")

	net.Start( "DuckyAdmin_ChatText" )
		net.WriteTable( tbl )
	
	if targets then net.Send( targets ) else net.Broadcast()  end
	for k, v in pairs(tbl) do
		if not (type(v) == "string") then table.remove(tbl, k) end
	end
	table.remove(tbl, 2)
	local String = table.concat(tbl)
	DuckyAdmin.Log("%s: %s", targets, String)
end

DuckyAdmin.NoTarget = function(Ply)
	DuckyAdmin.Message(true, Ply, Color(255, 255, 255), " No Target")
end
DuckyAdmin.MultiTarget = function(Ply)
	DuckyAdmin.Message(true, Ply, Color(255, 255, 255), " Multiple Targets")
end

DuckyAdmin.PH.TargetNames = function(Targets)
	local Rtn = {}
	for _, Ply in pairs(Targets) do
		table.Add(Rtn, Ply:NiceName())
		table.insert(Rtn, ", ")
	end
	table.remove(Rtn)
	return Rtn
end

function CheckRank(Plugin, Ply, Target)
	if IsValid(Ply) then
		local Rank = Ply:GetRank()
		if Plugin.NeedTarget then
			if Ply == Target then
				return Plugin.SelfRankLevel <= Rank
			else
				return Plugin.OthersRankLevel <= Rank
			end
		else
			return Plugin.SelfRankLevel <= Rank
		end
	else
		return true
	end
end

function CanTarget(Plugin, Ply, Targets)
	local Rank = Ply:GetRank()
	local ORL = Plugin.OthersRankLevel
	local SRL = Plugin.SelfRankLevel
	if #Targets == 1 and (Ply==Targets[1]) then
		return SRL <= Rank, {Ply}
	elseif #Targets > 0 then
		for Key, Target in pairs(Targets) do
			if IsValid(Ply) then
				if (Ply==Target) or (ORL >= Rank) or (Rank <= Target:GetRank()) then table.remove(Targets, Key) end
			end
		end
		if #Targets > 0 then return true, Targets else return false, {} end
	end
end

hook.Add("PlayerSay", "PluginHandler", function( Ply, Text, Tema )
	local Prefix = string.sub(Text, 0, 1)
	local Splode = string.Explode(" ", Text)
	local Command = string.sub(Splode[1], 2)
	local Args = table.Copy(Splode)
	table.remove(Args, 1)
	local DontShow = DuckyAdmin.PH.TryCommand(Ply, Splode, Prefix, Command, Args, false)
	if DontShow then return "" end
end)

concommand.Add("DA", function(Ply, _, Ars, Str)
	local Command = Ars[1]
	local Args = table.Copy(Ars)
	table.remove(Args, 1)
	DuckyAdmin.PH.TryCommand(Ply, Ars, "!", Command, Args, true)
end)

for _, FileName in pairs(file.Find("DuckyAdmin/plugins/*.lua", "LUA")) do
	Msg("Loading "..FileName.."->")
	include("DuckyAdmin/plugins/"..FileName)
	Msg(" Loaded\n")
end
