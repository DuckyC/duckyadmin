--[[   _                                
    ( )                               
   _| |   __   _ __   ___ ___     _ _
 /'_` | /'__`\( '__)/' _ ` _ `\ /'_` )
( (_| |(  ___/| |   | ( ) ( ) |( (_| |
`\__,_)`\____)(_)   (_) (_) (_)`\__,_)

--]]

local PANEL = {}

--[[---------------------------------------------------------
   Name: Init
-----------------------------------------------------------]]
function PANEL:Init()
    self:SetTextInset( 5, 0 )
	self.Name = "DListViewLabelE"
    self:SetHistoryEnabled( false )
    self.History = {}
    self.HistoryPos = 0
    
    self:SetPaintBorderEnabled( false )
    self:SetPaintBackgroundEnabled( false )

    self:SetDrawBorder( false )
    self:SetDrawBackground( true )
    self:SetEnterAllowed( true )
    self:SetUpdateOnType( false )
    self:SetNumeric( false )
    self:SetTall( 20 )
	
    self.m_bLoseFocusOnClickAway = true
    
    self:SetCursor( "beam" )
    
    derma.SkinHook( "Scheme", "TextEntry", self )

    self:SetFont( "DermaDefault" )
end

function PANEL:UpdateColours( skin )
    if ( self:GetParent():IsLineSelected() )    then return self:SetTextStyleColor( skin.Colours.Label.Bright ) end
    return self:SetTextStyleColor( skin.Colours.Label.Dark )
end

derma.DefineControl( "DListViewLabelE", "", PANEL, "DTextEntry" )

local PANEL = {}

Derma_Hook( PANEL, "Paint", "Paint", "ListViewLine" )
Derma_Hook( PANEL, "ApplySchemeSettings", "Scheme", "ListViewLine" )
Derma_Hook( PANEL, "PerformLayout", "Layout", "ListViewLine" )

AccessorFunc( PANEL, "m_iID",                 "ID" )
AccessorFunc( PANEL, "m_pListView",         "ListView" )
AccessorFunc( PANEL, "m_bAlt",                 "AltLine" )

--[[---------------------------------------------------------
   Name: Init
-----------------------------------------------------------]]
function PANEL:Init()
	self.Name = "DListView_LineE"
    self:SetSelectable( true )
    self:SetMouseInputEnabled( true )
    
    self.Columns = {}

end

--[[---------------------------------------------------------
   Name: OnSelect
-----------------------------------------------------------]]
function PANEL:OnSelect()

    -- For override
    
end

--[[---------------------------------------------------------
   Name: OnRightClick
-----------------------------------------------------------]]
function PANEL:OnRightClick()

    -- For override
    
end

--[[---------------------------------------------------------
   Name: OnMousePressed
-----------------------------------------------------------]]
function PANEL:OnMousePressed( mcode )

    if ( mcode == MOUSE_RIGHT ) then
    
        -- This is probably the expected behaviour..
        if ( !self:IsLineSelected() ) then
        
            self:GetListView():OnClickLine( self, true )
            self:OnSelect()

        end
        
        self:GetListView():OnRowRightClick( self:GetID(), self )
        self:OnRightClick()
        
        return
        
    end

    self:GetListView():OnClickLine( self, true )
    self:OnSelect()
    
end

--[[---------------------------------------------------------
   Name: OnMousePressed
-----------------------------------------------------------]]
function PANEL:OnCursorMoved()

    if ( input.IsMouseDown( MOUSE_LEFT ) ) then
        self:GetListView():OnClickLine( self )
    end
    
end

--[[---------------------------------------------------------
   Name: IsLineSelected
-----------------------------------------------------------]]
function PANEL:IsLineSelected()

    return self.m_bSelected

end

--[[---------------------------------------------------------
   Name: SetColumnText
-----------------------------------------------------------]]
function PANEL:SetColumnText( i, strText )

    if ( type( strText ) == "Panel" ) then
    
        if ( IsValid( self.Columns[ i ] ) ) then self.Columns[ i ]:Remove() end
    
        strText:SetParent( self )
        self.Columns[ i ] = strText
        self.Columns[ i ].Value = strText
        return
        
    end

    if ( !IsValid( self.Columns[ i ] ) ) then
        self.Columns[ i ] = vgui.Create( "DListViewLabelE", self )
        self.Columns[ i ]:SetMouseInputEnabled( true )
		self.Columns[ i ].OnEnter = function()
			self:TextOnEnter(i, self.Columns[ i ]:GetValue() )
		end
        
    end
	
    self.Columns[ i ]:SetText( tostring( strText ) )
    self.Columns[ i ].Value = strText
    return self.Columns[ i ]

end

PANEL.SetValue = PANEL.SetColumnText


--[[---------------------------------------------------------
   Name: SetColumnText
-----------------------------------------------------------]]
function PANEL:GetColumnText( i )

    if ( !self.Columns[ i ] ) then return "" end
    
    return self.Columns[ i ].Value

end

PANEL.GetValue = PANEL.GetColumnText

--[[---------------------------------------------------------
   Name: SetColumnText
-----------------------------------------------------------]]
function PANEL:DataLayout( ListView )

    self:ApplySchemeSettings()

    local height = self:GetTall()
    
    local x = 0
    for k, Column in pairs( self.Columns ) do
    
        local w = ListView:ColumnWidth( k )
        Column:SetPos( x, 0 )
        Column:SetSize( w, height )
        x = x + w
    
    end    

end
function PANEL:TextOnEnter(Column, NewText )
	print("Line", self.Name, Column)
	self:GetParent():GetParent():TextOnEnter(self, Column, NewText )
end
derma.DefineControl( "DListView_LineE", "A line from the List View", PANEL, "Panel" )

    
local PANEL = {}

AccessorFunc( PANEL, "m_bDirty",                 "Dirty",                 FORCE_BOOL )
AccessorFunc( PANEL, "m_bSortable",             "Sortable",             FORCE_BOOL )

AccessorFunc( PANEL, "m_iHeaderHeight",         "HeaderHeight" )
AccessorFunc( PANEL, "m_iDataHeight",             "DataHeight" )

AccessorFunc( PANEL, "m_bMultiSelect",             "MultiSelect" )
AccessorFunc( PANEL, "m_bHideHeaders",             "HideHeaders" )

Derma_Hook( PANEL, "Paint", "Paint", "ListView" )


--[[---------------------------------------------------------
   Name: Init
-----------------------------------------------------------]]
function PANEL:Init()
	self.Name = "DListViewE"
    self:SetSortable( true )
    self:SetMouseInputEnabled( true )
    self:SetMultiSelect( true )
    self:SetHideHeaders( false )

    self:SetDrawBackground( true )
    self:SetHeaderHeight( 16 )
    self:SetDataHeight( 17 )
    
    self.Columns = {}
    
    self.Lines = {}
    self.Sorted = {}
    
    self:SetDirty( true )
    
    self.pnlCanvas     = vgui.Create( "Panel", self )
    
    self.VBar         = vgui.Create( "DVScrollBar", self )
    self.VBar:SetZPos( 20 )

end

--[[---------------------------------------------------------
   Name: DisableScrollbar
-----------------------------------------------------------]]
function PANEL:DisableScrollbar()

    if ( IsValid( self.VBar ) ) then
        self.VBar:Remove()
    end
    
    self.VBar = nil

end

--[[---------------------------------------------------------
   Name: GetLines
-----------------------------------------------------------]]
function PANEL:GetLines()
    return self.Lines
end



--[[---------------------------------------------------------
   Name: GetInnerTall
-----------------------------------------------------------]]
function PANEL:GetInnerTall()
    return self:GetCanvas():GetTall()
end

--[[---------------------------------------------------------
   Name: GetCanvas
-----------------------------------------------------------]]
function PANEL:GetCanvas()
    return self.pnlCanvas
end

--[[---------------------------------------------------------
   Name: AddColumn
-----------------------------------------------------------]]
function PANEL:AddColumn( strName, strMaterial, iPosition )

    local pColumn = nil
    
    if ( self.m_bSortable ) then
        pColumn = vgui.Create( "DListView_Column", self )
    else
        pColumn = vgui.Create( "DListView_ColumnPlain", self )
    end
        pColumn:SetName( strName )
		pColumn.Text = strName
        pColumn:SetMaterial( strMaterial )
        pColumn:SetZPos( 10 )

        
    if ( iPosition ) then
    
        -- Todo, insert in specific position
        
    else
    
        local ID = table.insert( self.Columns, pColumn )
        pColumn:SetColumnID( ID )
    
    end

    self:InvalidateLayout()
    
    return pColumn
    
end

--[[---------------------------------------------------------
   Name: RemoveLine
-----------------------------------------------------------]]
function PANEL:RemoveLine( LineID )

    local Line = self:GetLine( LineID )
    local SelectedID = self:GetSortedID( LineID )
    
    self.Lines[ LineID ] = nil
    table.remove( self.Sorted, SelectedID )

    self:SetDirty( true )
    self:InvalidateLayout()
    
    Line:Remove()

end

--[[---------------------------------------------------------
   Name: ColumnWidth
-----------------------------------------------------------]]
function PANEL:ColumnWidth( i )

    local ctrl = self.Columns[ i ]
    if (!ctrl) then return 0 end
    
    return ctrl:GetWide()

end

--[[---------------------------------------------------------
   Name: FixColumnsLayout
-----------------------------------------------------------]]
function PANEL:FixColumnsLayout()

    local NumColumns = #self.Columns
    if ( NumColumns == 0 ) then return end

    local AllWidth = 0
    for k, Column in pairs( self.Columns ) do
        AllWidth = AllWidth + Column:GetWide()
    end
    
    local ChangeRequired = self.pnlCanvas:GetWide() - AllWidth
    local ChangePerColumn = math.floor( ChangeRequired / NumColumns )
    local Remainder = ChangeRequired - (ChangePerColumn * NumColumns)
    
    for k, Column in pairs( self.Columns ) do

        local TargetWidth = Column:GetWide() + ChangePerColumn
        Remainder = Remainder + ( TargetWidth - Column:SetWidth( TargetWidth ) )
    
    end
    
    -- If there's a remainder, try to palm it off on the other panels, equally
    while ( Remainder != 0 ) do

        local PerPanel = math.floor( Remainder / NumColumns )
        
        for k, Column in pairs( self.Columns ) do
    
            Remainder = math.Approach( Remainder, 0, PerPanel )
            
            local TargetWidth = Column:GetWide() + PerPanel
            Remainder = Remainder + (TargetWidth - Column:SetWidth( TargetWidth ))
            
            if ( Remainder == 0 ) then break end
        
        end
        
        Remainder = math.Approach( Remainder, 0, 1 )
    
    end
        
    -- Set the positions of the resized columns
    local x = 0
    for k, Column in pairs( self.Columns ) do
    
        Column.x = x
        x = x + Column:GetWide()
        
        Column:SetTall( self:GetHeaderHeight() )
        Column:SetVisible( !self:GetHideHeaders() )
    
    end

end

--[[---------------------------------------------------------
   Name: Paint
-----------------------------------------------------------]]
function PANEL:PerformLayout()
    
    -- Do Scrollbar
    local Wide = self:GetWide()
    local YPos = 0
    
    if ( IsValid( self.VBar ) ) then
    
        self.VBar:SetPos( self:GetWide() - 16, 0 )
        self.VBar:SetSize( 16, self:GetTall() )
        self.VBar:SetUp( self.VBar:GetTall() - self:GetHeaderHeight(), self.pnlCanvas:GetTall() )
        YPos = self.VBar:GetOffset()

        if ( self.VBar.Enabled ) then Wide = Wide - 16 end
    
    end
    
    if ( self.m_bHideHeaders ) then
        self.pnlCanvas:SetPos( 0, YPos )
    else
        self.pnlCanvas:SetPos( 0, YPos + self:GetHeaderHeight() )
    end

    self.pnlCanvas:SetSize( Wide, self.pnlCanvas:GetTall() )
    
    self:FixColumnsLayout()
    
    --
    -- If the data is dirty, re-layout
    --
    if ( self:GetDirty( true ) ) then
    
        self:SetDirty( false )
        local y = self:DataLayout()
        self.pnlCanvas:SetTall( y )
        
        -- Layout again, since stuff has changed..
        self:InvalidateLayout( true )
    
    end
    


end



--[[---------------------------------------------------------
   Name: OnScrollbarAppear
-----------------------------------------------------------]]
function PANEL:OnScrollbarAppear()

    self:SetDirty( true )
    self:InvalidateLayout()

end

--[[---------------------------------------------------------
   Name: OnRequestResize
-----------------------------------------------------------]]
function PANEL:OnRequestResize( SizingColumn, iSize )
    
    -- Find the column to the right of this one
    local Passed = false
    local RightColumn = nil
    for k, Column in ipairs( self.Columns ) do
    
        if ( Passed ) then
            RightColumn = Column
            break
        end
    
        if ( SizingColumn == Column ) then Passed = true end
    
    end
    
    -- Alter the size of the column on the right too, slightly
    if ( RightColumn ) then
    
        local SizeChange = SizingColumn:GetWide() - iSize
        RightColumn:SetWide( RightColumn:GetWide() + SizeChange )
        
    end
    
    SizingColumn:SetWide( iSize )
    self:SetDirty( true )
    
    -- Invalidating will munge all the columns about and make it right
    self:InvalidateLayout()

end

--[[---------------------------------------------------------
   Name: DataLayout
-----------------------------------------------------------]]
function PANEL:DataLayout()

    local y = 0
    local h = self.m_iDataHeight
    
    for k, Line in ipairs( self.Sorted ) do
    
        Line:SetPos( 1, y )
        Line:SetSize( self:GetWide()-2, h )
        Line:DataLayout( self )
        
        Line:SetAltLine( k % 2 == 1 )
        
        y = y + Line:GetTall()
    
    end
    
    return y

end

--[[---------------------------------------------------------
   Name: AddLine - returns the line number.
-----------------------------------------------------------]]
function PANEL:AddLine( ... )

    self:SetDirty( true )
    self:InvalidateLayout()

    local Line = vgui.Create( "DListView_LineE", self.pnlCanvas )
    local ID = table.insert( self.Lines, Line )
    
    Line:SetListView( self )
    Line:SetID( ID )
    
    -- This assurs that there will be an entry for every column
    for k, v in pairs( self.Columns ) do
        Line:SetColumnText( k, "" )
    end

    for k, v in pairs( {...} ) do
        Line:SetColumnText( k, v )
    end
    
    -- Make appear at the bottom of the sorted list
    local SortID = table.insert( self.Sorted, Line )
    
    if ( SortID % 2 == 1 ) then
        Line:SetAltLine( true )
    end

    return Line
    
end

--[[---------------------------------------------------------
   Name: OnMouseWheeled
-----------------------------------------------------------]]
function PANEL:OnMouseWheeled( dlta )

    if ( !IsValid( self.VBar ) ) then return end
    
    return self.VBar:OnMouseWheeled( dlta )
    
end

--[[---------------------------------------------------------
   Name: OnMouseWheeled
-----------------------------------------------------------]]
function PANEL:ClearSelection( dlta )

    for k, Line in pairs( self.Lines ) do
        Line:SetSelected( false )
    end
    
end

--[[---------------------------------------------------------
   Name: GetSelectedLine
-----------------------------------------------------------]]
function PANEL:GetSelectedLine()

    for k, Line in pairs( self.Lines ) do
        if ( Line:IsSelected() ) then return k end
    end
    
end

--[[---------------------------------------------------------
   Name: GetLine
-----------------------------------------------------------]]
function PANEL:GetLine( id )

    return self.Lines[ id ]
    
end

--[[---------------------------------------------------------
   Name: GetSortedID
-----------------------------------------------------------]]
function PANEL:GetSortedID( line )

    for k, v in pairs( self.Sorted ) do
    
        if ( v:GetID() == line ) then return k end
    
    end
    
end

--[[---------------------------------------------------------
   Name: OnClickLine
-----------------------------------------------------------]]
function PANEL:OnClickLine( Line, bClear )

    local bMultiSelect = self.m_bMultiSelect
    if ( !bMultiSelect && !bClear ) then return end
    
    --
    -- Control, multi select
    --
    if ( bMultiSelect && input.IsKeyDown( KEY_LCONTROL ) ) then
        bClear = false
    end
    
    --
    -- Shift block select
    --
    if ( bMultiSelect && input.IsKeyDown( KEY_LSHIFT ) ) then
    
        local Selected = self:GetSortedID( self:GetSelectedLine() )
        if ( Selected ) then
        
            if ( bClear ) then self:ClearSelection() end
            
            local LineID = self:GetSortedID( Line:GetID() )
        
            local First = math.min( Selected, LineID )
            local Last = math.max( Selected, LineID )
            
            for id = First, Last do
            
                local line = self.Sorted[ id ]
                line:SetSelected( true )
            
            end
        
            return
        
        end
        
    end
    
    --
    -- Check for double click
    --
    if ( Line:IsSelected() && Line.m_fClickTime && (!bMultiSelect || bClear) ) then
    
        local fTimeDistance = SysTime() - Line.m_fClickTime

        if ( fTimeDistance < 0.3 ) then
            self:DoDoubleClick( Line:GetID(), Line )
            return
        end
    
    end

    --
    -- If it's a new mouse click, or this isn't
    --  multiselect we clear the selection
    --
    if ( !bMultiSelect || bClear ) then
        self:ClearSelection()
    end
    
    if ( Line:IsSelected() ) then return end

    Line:SetSelected( true )
    Line.m_fClickTime = SysTime()
    
    self:OnRowSelected( Line:GetID(), Line )
    
end

function PANEL:SortByColumns( c1, d1, c2, d2, c3, d3, c4, d4 )

    table.Copy( self.Sorted, self.Lines )
    
    table.sort( self.Sorted, function( a, b )
                                
                                if (!IsValid( a )) then return true end
                                if (!IsValid( b )) then return false end
                                
                                if ( c1 && a:GetColumnText( c1 ) != b:GetColumnText( c1 ) ) then
                                    if ( d1 ) then a, b = b, a end
                                    return a:GetColumnText( c1 ) < b:GetColumnText( c1 )
                                end
                                
                                if ( c2 && a:GetColumnText( c2 ) != b:GetColumnText( c2 ) ) then
                                    if ( d2 ) then a, b = b, a end
                                    return a:GetColumnText( c2 ) < b:GetColumnText( c2 )
                                end
                                    
                                if ( c3 && a:GetColumnText( c3 ) != b:GetColumnText( c3 ) ) then
                                    if ( d3 ) then a, b = b, a end
                                    return a:GetColumnText( c3 ) < b:GetColumnText( c3 )
                                end
                                
                                if ( c4 && a:GetColumnText( c4 ) != b:GetColumnText( c4 ) ) then
                                    if ( d4 ) then a, b = b, a end
                                    return a:GetColumnText( c4 ) < b:GetColumnText( c4 )
                                end
                                
                                return true                            
                        end )

    self:SetDirty( true )
    self:InvalidateLayout()

end

--[[---------------------------------------------------------
   Name: SortByColumn
-----------------------------------------------------------]]
function PANEL:SortByColumn( ColumnID, Desc )

    table.Copy( self.Sorted, self.Lines )
    
    table.sort( self.Sorted, function( a, b )

                                    if ( Desc ) then
                                        a, b = b, a
                                    end
            
                                    return a:GetColumnText( ColumnID ) < b:GetColumnText( ColumnID )
                            
                        end )

    self:SetDirty( true )
    self:InvalidateLayout()
    

end

--[[---------------------------------------------------------
   Name: SelectFirstItem
   Selects the first item based on sort..
-----------------------------------------------------------]]
function PANEL:SelectItem( Item )

    if ( !Item ) then return end
    
    Item:SetSelected( true )
    self:OnRowSelected( Item:GetID(), Item )

end

--[[---------------------------------------------------------
   Name: SelectFirstItem
   Selects the first item based on sort..
-----------------------------------------------------------]]
function PANEL:SelectFirstItem()

    self:ClearSelection()
    self:SelectItem( self.Sorted[ 1 ] )

end

--[[---------------------------------------------------------
   Name: DoDoubleClick
-----------------------------------------------------------]]
function PANEL:DoDoubleClick( LineID, Line )
end

--[[---------------------------------------------------------
   Name: OnRowSelected
-----------------------------------------------------------]]
function PANEL:OnRowSelected( LineID, Line )
end

--[[---------------------------------------------------------
   Name: OnRowRightClick
-----------------------------------------------------------]]
function PANEL:OnRowRightClick( LineID, Line )
end
--[[---------------------------------------------------------
   Name: Clear
-----------------------------------------------------------]]
function PANEL:Clear()

    for k, v in pairs( self.Lines ) do
        v:Remove()
    end
    
    self.Lines = {}
    self.Sorted = {}
    
    self:SetDirty( true )

end

--[[---------------------------------------------------------
   Name: GetSelected
-----------------------------------------------------------]]
function PANEL:GetSelected()

    local ret = {}
    
    for k, v in pairs( self.Lines ) do
        if ( v:IsLineSelected() ) then
            table.insert( ret, v )
        end
    end

    return ret

end


--[[---------------------------------------------------------
   Name: SizeToContents
-----------------------------------------------------------]]
function PANEL:SizeToContents( )
    self:SetHeight( self.pnlCanvas:GetTall() + self:GetHeaderHeight() )
end
function PANEL:TextOnEnter(Line, Column, NewText )
 
end
function PANEL:ClearColumns() 
	for k, v in pairs(self.Columns) do
		v:Remove()
	end
	self.Columns = {}
	self:InvalidateLayout()
	self:SetDirty( true )
end
function PANEL:ClearAll()
	self:Clear()
	self:ClearColumns() 
end
function PANEL:GetColumnNameFromID( ID )
	for _, Column in pairs(self.Columns) do
		if ID == Column:GetColumnID() then return Column.Header:GetValue() end
	end
	return ""
end

derma.DefineControl( "DListViewE", "Data View", PANEL, "DPanel" )