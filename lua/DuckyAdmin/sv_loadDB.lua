
local function LoadRanks(Q, Data)
	DuckyAdmin.SDB.Ranks[Data.ID] = {}
	DuckyAdmin.SDB.Ranks[Data.ID].Name = Data.Name
	local TVals = string.Explode(";", Data.Color)
	DuckyAdmin.SDB.Ranks[Data.ID].Color = Color(TVals[1], TVals[2], TVals[3])
	
	team.SetUp( Data.ID, Data.Name, DuckyAdmin.SDB.Ranks[Data.ID].Color )
end
DuckyAdmin.SDB.SQL.SendQuery("SELECT * FROM ranks", nil, LoadRanks)
DuckyAdmin.SDB.SQL.SendQuery("SELECT * FROM convars", nil, function(Q, Data)
	RunConsoleCommand(Data["Name"], Data["Value"])
	DuckyAdmin.Log("Set convar %s to %s", Data["Name"], Data["Value"])
end)

DuckyAdmin.SDB.SQL.SendQuery("SELECT * FROM settings", nil, function(Q, Data)
	DuckyAdmin.SDB.Settings[Data["Name"]] = Data["Value"]
	DuckyAdmin.Log("Set setting %s to %s", Data["Name"], Data["Value"])
end)


