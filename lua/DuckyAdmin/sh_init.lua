assert(type(DuckyAdmin)=="table")

for _, FileName in pairs(file.Find("DuckyAdmin/sh_*.lua", "LUA")) do
	if not FileName == "sh_init.lua" then 
		Msg("Loading "..FileName.." -> ")
		include("DuckyAdmin/"..FileName)
		Msg("Loaded \n")
	end
end
