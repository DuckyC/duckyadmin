DuckyAdmin.Utils = {}
local function NiceTime(WithDate)
	local T = os.date("*t", os.time())
	local Date = string.format("%02d/%02d/%02d ", T["day"], T["month"], T["year"])
	local Time = string.format("[%02d:%02d:%02d]: ", T["hour"], T["min"], T["sec"])
	if WithDate then return Date..Time else return Time end
end
DuckyAdmin.Utils.NiceTime = NiceTime
local function Log( FormatString, ...)
	if true then
		local FTime = NiceTime(true)
		if not file.Exists("DuckyAdmin", "DATA") then
			file.CreateDir("DuckyAdmin")
		else
			local LogFile = file.Open("DuckyAdmin/Log.txt", "a", "DATA")
			if not LogFile then
				file.Write("DuckyAdmin/Log.txt", FTime.."Creation of the log.txt")
				print(FTime.."Creation of the log.txt")
				LogFile = file.Open("DuckyAdmin/Log.txt", "a", "DATA")
			end
			LogFile:Write(FTime..string.format(FormatString, ...).."\n")
			local FTime = NiceTime(false)
			print(FTime..string.format(FormatString, ...))
			LogFile:Close()
		end
	end
end
Log("---LOADED UTILS---")
DuckyAdmin.Log = Log

local function FindBySteamID(SteamID)
	for k, v in pairs(player.GetAll()) do
		if v:SteamID() == SteamID then return v end
	end
end
DuckyAdmin.Utils.FindBySteamID = FindBySteamID

local function CreateFakePlayer(Data)
	local Tbl = {}
	Tbl.Nickname = Data.name
	Tbl.UID = ""
	Tbl.SteamID = Data.networkid
	Tbl.Rank = 1
	Tbl.LastIP = "0.0.0.0"
	return Tbl
end
DuckyAdmin.Utils.CreateFakePlayer = CreateFakePlayer