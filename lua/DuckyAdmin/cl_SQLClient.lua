DuckyAdmin.SQLClient = {}
DuckyAdmin.SQLClient.Data = {}
include("cl_DViewListEdit.lua")
local Panel
local Tables
local Data

local function Reload(Btn)
	net.Start("DuckyAdmin_RequestSQL") 
	net.SendToServer()
	Data:ClearAll()
	Tables:Clear()
end
local function RemovePanel()
	if not (Panel == nil) then Panel:Remove() end
end
local function CreatePanel()
	DuckyAdmin.SQLClient.CurrentTable = ""
	Panel = vgui.Create( "DFrame" ) 
	Panel:SetSize( 1000, 500 ) 
	Panel:SetTitle( "SQL Database Client" )
	Panel:SetDraggable( true ) 
	Panel:ShowCloseButton( true ) 
	Panel:Center()
 
	Tables = vgui.Create("DListView", Panel)
	Tables:SetPos(10, 32)
	Tables:SetSize(150, 400)
	Tables:SetMultiSelect(false)
	Tables:AddColumn("Name")
	Tables.OnRowSelected = function( Panel , Line )
		Data:ClearAll()
		local LineValue = Panel:GetLine(Line):GetValue(1)
		local Tbl = DuckyAdmin.SQLClient.Data[LineValue]
		DuckyAdmin.SQLClient.CurrentTable = LineValue
		if Tbl and #Tbl > 0 then
			local Columns = {}
			Data:AddColumn("ID")
			for k, v in pairs(Tbl[1]) do
				if not (k == "ID") then Data:AddColumn(k) end
			end
			for k, v in pairs(Tbl) do
				local Values = {}
				local ID = ""
				for k, v in pairs(v) do
					if k == "ID" then 
						ID = v 
					else
						table.insert(Values, v)
					end
				end
				Data:AddLine(ID, unpack(Values))
			end
		end
	end
	
	local ReloadButton
 
	local ReloadButton = vgui.Create( "DButton", Panel )
	ReloadButton:SetSize( 150, 40 )
	ReloadButton:SetPos( 10, 442 )
	ReloadButton:SetText( "Reload" )
	ReloadButton.DoClick = Reload
	
	local PSheet = vgui.Create( "DPropertySheet", Panel )
	PSheet:SetPos(170, 32)
	PSheet:SetSize(820, 450)
	 
	local DataPanel = vgui.Create( "DPanel" )
	DataPanel:SetSize(815, 397)
	DataPanel.Paint = function(self)
		surface.SetDrawColor( 50, 50, 50, 255 ) 
		surface.DrawRect( 0, 0, self:GetWide(), self:GetTall() )
	end
		Data = vgui.Create("DListViewE", DataPanel)
		Data:SetPos(5, 5)
		Data:SetSize(795, 402)
		Data:SetMultiSelect(false)
		Data.TextOnEnter = function(self, Line, Column, NewText ) 
			local ValueName = Data:GetColumnNameFromID(Column)
			local TableName = DuckyAdmin.SQLClient.CurrentTable
			local ID = Line:GetValue(1)
			DuckyAdmin.SQLClient.SendQuery("UPDATE %s SET %s=%q WHERE %s=%q", TableName, ValueName, NewText, "ID", ID)
		end
	 
	local PlayerOptions = vgui.Create( "DPanel" )
	DataPanel:SetSize(815, 397)
	PlayerOptions.Paint = function(self)
		surface.SetDrawColor( 50, 50, 50, 255 ) 
		surface.DrawRect( 0, 0, self:GetWide(), self:GetTall() )
	end
		Players = vgui.Create("DListView", PlayerOptions)
		Players:SetPos(5, 5)
		Players:SetSize(350, 402)
		Players:SetMultiSelect(false)
		Players:AddColumn("Name")
		Players:AddColumn("ID")
		Players:AddColumn("Rank")
		for _, Ply in pairs(player.GetAll()) do
			Players:AddLine(Ply:GetName(), Ply:EntIndex(), Ply:GetRank())
		end
		
		local UpdateSQL = vgui.Create( "DButton", PlayerOptions)
		UpdateSQL:SetPos( 365, 5 )
		UpdateSQL:SetText( "Update Player On SQL" )
		UpdateSQL:SetSize( 120, 60 )
		UpdateSQL.DoClick = function()
			local Targets = {}
			for Key, Line in pairs(Players:GetSelected()) do
				local Ply = Entity(tonumber(Line:GetValue(2))) 
				table.insert(Targets, Ply)
			end
			if #Targets > 0 then net.Start("DuckyAdmin_UpdatePlayerOnSQL") net.WriteTable(Targets) net.SendToServer() end
		end
			 
		local UpdateServer = vgui.Create( "DButton", PlayerOptions )
		UpdateServer:SetPos( 365, 70 )
		UpdateServer:SetText( "Update Player On Server" )
		UpdateServer:SetSize( 120, 60 )
		UpdateServer.DoClick = function()
			local Targets = {}
			for Key, Line in pairs(Players:GetSelected()) do
				local Ply = Entity(tonumber(Line:GetValue(2))) 
				table.insert(Targets, Ply)
			end
			if #Targets > 0 then net.Start("DuckyAdmin_UpdatePlayerOnServer") net.WriteTable(Targets) net.SendToServer() end
		end
		
	PSheet:AddSheet( "Tables", DataPanel, "gui/silkicons/computer", false, false, "Panel with the table contents" )
	PSheet:AddSheet( "Player Options", PlayerOptions, "gui/silkicons/group", false, false, "Player Options" )
	
end
net.Receive("DuckyAdmin_SendSQL", function()
	local TableName = net.ReadString()
	local Table = net.ReadTable()
	DuckyAdmin.SQLClient.Data[TableName] = Table
	Tables:AddLine(TableName)
end)
net.Receive("DuckyAdmin_DoSQLRecall", function()
	local Success = net.ReadBit()
	if Success == 1 then
		print("Success")
		local Data = net.ReadTable()
		PrintTable(Data)
	elseif Success == 0 then
		local Err = net.ReadString()
		print(string.format("Query Failed[%q]", Err))
	end
end)
DuckyAdmin.SQLClient.SendQuery = function(String, ...)
	net.Start("DuckyAdmin_DoSQL")
		local String = string.format(String, ...)
		print("string", String)
		net.WriteString(String)
	net.SendToServer()
end
net.Receive("DuckyAdmin_OpenSQLClient", function()
	RemovePanel()
	CreatePanel()
	Reload()
	Panel:MakePopup()
end)

