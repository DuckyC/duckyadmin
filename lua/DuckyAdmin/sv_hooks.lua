gameevent.Listen("player_connect")
gameevent.Listen("player_disconnect")

hook.Add( "player_connect", "LoadData", function(Data)
	local IP = string.Explode(":", Data.address)[1]
	DuckyAdmin.Message(false, Data.name," connected[", IP, "]")
	DuckyAdmin.Log("User connected %s[%s, %s]", Data.name, Data.networkid, Data.address)
	DuckyAdmin.SDB.SQL.LoadData( Data.networkid, Data)
	DuckyAdmin.Ban.CheckBan( Data.networkid )
end )
local function HandleUser( Ply )
	if DuckyAdmin.SDB.NewUsers[Ply:SteamID()] then
		Ply:CreatePlayerOnSQL()
		DuckyAdmin.SDB.NewUsers[Ply:SteamID()] = nil
		DuckyAdmin.Log("Created New User")
		Ply:UpdatePlayerOnServer()
	else
		Ply:UpdatePlayerOnSQL()
	end
	if DuckyAdmin.SDB.Players[Ply:SteamID()] then
		timer.Simple(5, function()
			Ply:SetUserGroup(Ply:GetRankName()) 
			Ply:SetTeam(Ply:GetRank())
			print(Ply, Ply:GetRank(), Ply:GetRankName())
		end)
	else
		DuckyAdmin.Log("YOU FAIL LOADING PLAYER %s ", Ply)
	end


	
	
	DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " joined the game!")
end
hook.Add( "PlayerInitialSpawn", "HandleUser", HandleUser )

hook.Add("player_disconnect", "DeleteTempUserInfo", function(data)
	DuckyAdmin.Log("User disconnected %s[%s, %s]", data.name, data.networkid, data.address)
	local SteamID = data.networkid
	if DuckyAdmin.SDB.Players[SteamID].UpdatedNick then
		DuckyAdmin.SDB.SQL.SendQuery(string.format("UPDATE users SET Nickname=%q WHERE SteamID = %q", DuckyAdmin.SDB.Players[SteamID].Nickname, SteamID), function() end)
	end
	DuckyAdmin.SDB.Players[SteamID] = nil
	
	net.Start("DuckyAdmin_UpdatePlayers")
	net.WriteTable(DuckyAdmin.SDB.Players)
	net.Broadcast()
end)
hook.Add("PlayerDisconnected", "Say goodbye", function(Ply)
	DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " left the game!")
end)
net.Receive("DuckyAdmin_SendDB", function(L, Ply)
	net.Start("DuckyAdmin_FillDB")
	net.WriteTable(DuckyAdmin.SDB.Players)
	net.WriteTable(DuckyAdmin.SDB.Ranks)
	net.Send(Ply)
	print("DuckyAdmin_FillDB")
	DuckyAdmin.SendPlayers()
	for PluginName, ClientFunction in pairs(DuckyAdmin.PH.Clientcode) do
		DuckyAdmin.RunLua(ClientFunction)
	end		
end)

net.Receive("DuckyAdmin_RequestSQL", function(Len, Client)
	DuckyAdmin.Log("Got RequestSQL from %s[%s, %s]", Client:GetName(), Client:SteamID(), Client:IPAddress())
	local Rank = Client:GetRank()
	if Rank >= 10 then
		for _, TableName in pairs({"bans", "ranks", "convars", "users", "settings"}) do
			local function Callback(Q, Data)
				net.Start("DuckyAdmin_SendSQL")
				net.WriteString(TableName)
				net.WriteTable(Data)
				net.Send( Client )
			end
			DuckyAdmin.SDB.SQL.SendQuery(string.format("SELECT * FROM %s", TableName), Callback)
		end
	end
end)
net.Receive("DuckyAdmin_DoSQL", function(Len, Client)
	DuckyAdmin.Log("Got DoSQL from %s[%s, %s]", Client:GetName(), Client:SteamID(), Client:IPAddress())
	local Rank = Client:GetRank()
	if Rank >= 10 then
		local function Callback(Q, Data)
			net.Start("DuckyAdmin_DoSQLRecall")
				net.WriteBit( true )
				net.WriteTable(Data)
			net.Send( Client )
		end
		local function ErrorC(Q, Err)
			net.Start("DuckyAdmin_DoSQLRecall")
				net.WriteBit( false )
				net.WriteString( Err )
			net.Send( Client )
		end
		local Query = net.ReadString()
		DuckyAdmin.SDB.SQL.SendQuery(Query, Callback, nil, ErrorC)
	end
end)
net.Receive("DuckyAdmin_UpdatePlayerOnSQL", function(Len, Client) 
	if Client:GetRank() >= 10 then
		for _, Ply in pairs(net.ReadTable()) do 
			Ply:UpdatePlayerOnSQL()
		end
	end
end)
net.Receive("DuckyAdmin_UpdatePlayerOnServer", function(Len, Client) 
	if Client:GetRank() >= 10 then
		for _, Ply in pairs(net.ReadTable()) do 
			Ply:UpdatePlayerOnServer()
		end
	end
end)
print("updated", os.time())