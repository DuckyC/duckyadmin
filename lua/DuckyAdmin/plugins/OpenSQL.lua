local PLUGIN = {}
PLUGIN.Name = "OpenSQL"
PLUGIN.Command = "OpenSQL"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 10
PLUGIN.OthersRankLevel = 10
PLUGIN.NeedTarget = false

PLUGIN.RunFunction = function( Ply, Args, Targets, InConsole)
	net.Start("DuckyAdmin_OpenSQLClient")
	net.Send(Ply)
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)
