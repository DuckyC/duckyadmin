local PLUGIN = {}
PLUGIN.Name = "UpdatePlugin"
PLUGIN.Hooks = {}
PLUGIN.Command = "UpdatePlugin"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 10
PLUGIN.OthersRankLevel = 10
PLUGIN.NeedTarget = false

PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	if file.Exists("addons/DuckyAdmin/lua/DuckyAdmin/plugins/"..Args[1]..".lua", "GAME") then
		include("DuckyAdmin/plugins/"..Args[1]..".lua")
		DuckyAdmin.Message(true, Ply, Color(255, 255, 255), "Updated ", Args[1])
	else
		DuckyAdmin.Message(true, Ply, Color(255, 255, 255), "File ", Args[1], " does not exist")
	end
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)