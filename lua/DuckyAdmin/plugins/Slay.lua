local PLUGIN = {}
PLUGIN.Name = "Slay"
PLUGIN.Command = "Slay"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 5
PLUGIN.OthersRankLevel = 6
PLUGIN.NeedTarget = true

PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	if #Targets > 0 then
		for _, TPly in pairs(Targets) do
			TPly:Kill()
		end
		if not Silent then
			if IsValid(Ply) then
				DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " Has Slayed ", unpack(Targets))
			else
				DuckyAdmin.Message(false, Color(0, 150, 255), "Console ", Color(255, 255, 255), " Has Slayed ", unpack(Targets))
			end
		end
	else
		DuckyAdmin.NoTarget(Ply)
	end
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)