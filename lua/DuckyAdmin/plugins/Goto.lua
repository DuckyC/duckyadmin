local PLUGIN = {}
PLUGIN.Name = "Goto"
PLUGIN.Command = "Goto"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 4
PLUGIN.OthersRankLevel = 4
PLUGIN.NeedTarget = true

-- Generate offsets
local positions = {}
for i=0,360,45 do
	positions[#positions+1] = Vector(math.cos(math.rad(i)),math.sin(math.rad(i)),0)
end
positions[#positions+1] = Vector(0,0,1)

local function findPosition( ply )
	local maxsize, minsize = ply:GetPhysicsObject():GetAABB()
	local size = minsize - maxsize
	
	local StartPos = ply:GetPos() + Vector(0,0,size.z/2)
	
	for _,v in ipairs( positions ) do
		local Pos = StartPos + v * size * 1.5
		
		local tr = {}
		tr.start = Pos
		tr.endpos = Pos
		tr.mins = size / 2 * -1
		tr.maxs = size / 2
		local trace = util.TraceHull( tr )
		
		if (!trace.Hit) then
			return Pos - Vector(0,0,size.z/2)
		end
	end
	
	return false
end

PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	if #Targets == 1 then
		local Target = Targets[1]
		
		local position = findPosition(Target)
		if position then
			Ply:SetPos(findPosition(Target))
		else -- if we're unable to find a good position, just force them there somewhere
			Ply:SetPos(Target:GetPos() + Vector(0, 100, 5))
		end
			
		if not Silent then
			DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " Has Teleported to ", Target)
		end
	elseif #Targets > 1 then
		DuckyAdmin.MultiTarget(Ply)
	elseif #Targets == 0 then
		DuckyAdmin.NoTarget(Ply)
	end
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)