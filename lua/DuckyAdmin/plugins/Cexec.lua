local PLUGIN = {}
PLUGIN.Name = "Cexec"
PLUGIN.Command = "Cexec"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 4
PLUGIN.OthersRankLevel = 8
PLUGIN.NeedTarget = true

PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	if #Targets > 0 then
		local Command = table.concat(Args, " ")
		for _, Target in ipairs( Targets ) do
			DuckyAdmin.RunLua([[LocalPlayer():ConCommand("]]..Command..[[")]], Target)
		end
		if not Silent then
			if IsValid(Ply) then
				DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " Has Run ", Command, " On ", unpack(Targets))
			else
				DuckyAdmin.Message(false, Color(0, 150, 255), "Console ", Color(255, 255, 255), " Has Run ", Command, " On ", unpack(Targets))
			end
		end
	else
		DuckyAdmin.NoTarget(Ply)
	end
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)