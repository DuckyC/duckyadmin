local PLUGIN = {}
PLUGIN.Name = "Ban"
PLUGIN.Command = "Ban"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 7
PLUGIN.OthersRankLevel = 7
PLUGIN.NeedTarget = true

PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	if #Targets == 1 then
		local Target = Targets[1]
		local Reason = Args[2] or "Banned"
		local Time = DuckyAdmin.Ban.Parse(Args[1])
		local TargetSteamID 
		local TargetIsPlayer = false
		if type(Target) == "string" then TargetSteamID = Target TargetIsPlayer = false else TargetSteamID = Target:SteamID()  TargetIsPlayer = true end
		local BannedBy
		if IsValid(Ply) then BannedBy = Ply:SteamID() else BannedBy = "Console" end
		
		local NiceTime = DuckyAdmin.Ban.BanPlayer(TargetSteamID, Time, Reason, BannedBy)
		print(TargetSteamID, Time, Reason, SteamID)
		if not Silent then
			if IsValid(Ply) then
				if TargetIsPlayer then
					DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " Has Banned ", Target, ": "..NiceTime)
				else
					DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " Has Banned ", TargetSteamID, ": "..NiceTime)
				end
			else
				if TargetIsPlayer then
					DuckyAdmin.Message(false, Color(0, 150, 255), "Console ", Color(255, 255, 255), " Has Banned ", Target, ": "..NiceTime)
				else
					DuckyAdmin.Message(false, Color(0, 150, 255), "Console ", Color(255, 255, 255), " Has Banned ", TargetSteamID, ": "..NiceTime)
				end
			end
		end
	elseif #Targets > 1 then
		DuckyAdmin.MultipleTargets(Ply)
	elseif #Targets == 0 then
		DuckyAdmin.NoTarget(Ply)
	end
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)