local PLUGIN = {}
PLUGIN.Name = "Bring"
PLUGIN.Command = "Bring"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 5
PLUGIN.OthersRankLevel = 5
PLUGIN.NeedTarget = true

-- Generate offsets
local positions = {}
for i=0,360,45 do
	positions[#positions+1] = Vector(math.cos(math.rad(i)),math.sin(math.rad(i)),0)
end
positions[#positions+1] = Vector(0,0,1)

local function findPosition( ply )
	local maxsize, minsize = ply:GetPhysicsObject():GetAABB()
	local size = minsize - maxsize
	
	local StartPos = ply:GetPos() + Vector(0,0,size.z/2)
	
	for _,v in ipairs( positions ) do
		local Pos = StartPos + v * size * 1.5
		
		local tr = {}
		tr.start = Pos
		tr.endpos = Pos
		tr.mins = size / 2 * -1
		tr.maxs = size / 2
		local trace = util.TraceHull( tr )
		
		if (!trace.Hit) then
			return Pos - Vector(0,0,size.z/2)
		end
	end
	
	return false
end

PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	if #Targets > 0 then
		local cantFindPos = false
		for _, Target in pairs( Targets ) do
			if cantFindPos then -- if there's no point in trying to look for free space
				Target:SetPos(Ply:GetPos() + Vector(0, 100, 5))
			else
				local position = findPosition( Ply )
				if position then
					Target:SetPos( position )
				else -- If there's no free space, just force them there
					Target:SetPos(Ply:GetPos() + Vector(0, 100, 5))
					cantFindPos = true -- And tell the rest of this loop that there's no point in trying to find any free space
				end
			end
		end

		if not Silent then
			DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " Has Brought ", unpack(Targets))
		end
	else
		DuckyAdmin.NoTarget(Ply)
	end
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)