local PLUGIN = {}
PLUGIN.Name = "PlayerPickup"
PLUGIN.Hooks = {}

PLUGIN.Hooks["PhysgunPickup"] = function(Ply, Ent) 
	local EntRank
	local PlyRank = Ply:GetRank()
	if Ent:GetClass():lower() == "player" then EntRank = Ent:GetRank() else EntRank = 1 end
	if PlyRank >= 8 and PlyRank > EntRank and Ent:GetClass():lower() == "player" then
		return true 
	end
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN) 