local PLUGIN = {}
PLUGIN.Name = "TP"
PLUGIN.Command = "TP"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 4
PLUGIN.OthersRankLevel = 5
PLUGIN.NeedTarget = false



PLUGIN.RunFunction = function( ply, Args, Targets, Silent)
	local tr = {}
	tr.start = ply:GetShootPos()
	tr.endpos = ply:GetShootPos() + ply:GetAimVector() * 100000000000
	tr.filter = ply
	local trace = util.TraceEntity( tr, ply )
	
	local normaltrace = ply:GetEyeTraceNoCursor()
		
	local maxsize, minsize = ply:GetPhysicsObject():GetAABB()
	local size = minsize - maxsize
	
	if trace.HitPos:Distance(normaltrace.HitPos) > size:Length() then -- The player is trying to teleport through a narrow hole
		trace = normaltrace
		trace.HitPos = trace.HitPos + trace.HitNormal * size * 1.2
	end
	
	ply:SetPos( trace.HitPos )
	ply:SetLocalVelocity( Vector(0,0,0) )
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)