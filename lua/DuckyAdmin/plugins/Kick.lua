local PLUGIN = {}
PLUGIN.Name = "Kick"
PLUGIN.Command = "kick"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 1
PLUGIN.OthersRankLevel = 6
PLUGIN.NeedTarget = true

PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	if #Targets > 0 then
		local Reason = "Kicked"
		if #Args > 0 then Reason = table.concat(Args, " ") end
		for _, TPly in pairs(Targets) do
			if Ply:GetRank() > TPly:GetRank() then 
				TPly:Kick(Reason)
			else  
				for I, TTPly in pairs(Targets) do
					if TTPly == TPly then table.remove(Targets, I) end
				end
			end
		end
		if not Silent then
			if IsValid(Ply) then
				DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " Has Kicked ", unpack(Targets), ": "..Reason)
			else
				DuckyAdmin.Message(false, Color(0, 150, 255), "Console ", Color(255, 255, 255), " Has Kicked ", unpack(Targets), ": "..Reason)
			end
		end
	else
		DuckyAdmin.NoTarget(Ply)
	end
end


DuckyAdmin.PH.SubmitPlugin(PLUGIN)