local PLUGIN = {}
PLUGIN.Name = "Slap"
PLUGIN.Command = "Slap"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 4
PLUGIN.OthersRankLevel = 6
PLUGIN.NeedTarget = true

PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	if #Targets > 0 then
		local Damage = math.abs(tonumber(Args[1]) or 10)
		for _, Target in ipairs( Targets ) do
			Target:SetHealth( Target:Health() - Damage )
			Target:ViewPunch( Angle( -10, 0, 0 ) )
			if ( Target:Health() < 1 ) then Target:Kill() end
		end
		if not Silent then
			if IsValid(Ply) then
				DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " Has Slapped ", unpack(Targets), Color(255, 255, 255), " With "..Damage)
			else
				DuckyAdmin.Message(false, Color(0, 150, 255), "Console ", Color(255, 255, 255), " Has Slapped ", unpack(Targets), Color(255, 255, 255) " With "..Damage)
			end
		end
	else
		DuckyAdmin.NoTarget(Ply)
	end
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)