local PLUGIN = {}
PLUGIN.Name = "SetRank"
PLUGIN.Command = "SetRank"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 10
PLUGIN.OthersRankLevel = 10
PLUGIN.NeedTarget = true

PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	if #Targets == 1 then
		if type(tonumber(Args[1])) == "number" then
			local Target = Targets[1]
			local NewRank = tonumber(Args[1])
			DuckyAdmin.SDB.Players[Target:SteamID()].Rank = NewRank
			Target:SetTeam(NewRank)
			Target:UpdatePlayerOnSQL()
			if not Silent then
				if IsValid(Ply) then
					DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " Has set ", Target, Color(255, 255, 255), "'s Rank to ", DuckyAdmin.SDB.Ranks[NewRank].Color, DuckyAdmin.SDB.Ranks[NewRank].Name)
				else
					DuckyAdmin.Message(false, Color(0, 150, 255), "Console ", Color(255, 255, 255), " Has set ", Target, Color(255, 255, 255),"'s Rank to ", DuckyAdmin.SDB.Ranks[NewRank].Color, DuckyAdmin.SDB.Ranks[NewRank].Name)
				end
			end
		end
	elseif #Targets == 0 then
		DuckyAdmin.NoTarget(Ply)
	end
end


DuckyAdmin.PH.SubmitPlugin(PLUGIN)