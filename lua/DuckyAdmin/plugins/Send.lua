local PLUGIN = {}
PLUGIN.Name = "Send"
PLUGIN.Command = "Send"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 6
PLUGIN.OthersRankLevel = 6
PLUGIN.NeedTarget = true

PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	if #Targets == 2 then
		local ToPerson = DuckyAdmin.PH.GetPlayers(Args[1])[1]
		Targets[1]:SetPos(ToPerson:GetPos() + Vector(0, 100, 5))
		if not Silent then
			DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " Has Sent ", Targets[1], " To ", ToPerson)
		end
	else
		DuckyAdmin.NoTarget(Ply)
	end
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)