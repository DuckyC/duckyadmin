local PLUGIN = {}
PLUGIN.Name = "PlayX"
PLUGIN.Hooks = {}
PLUGIN.Command = "PlayX"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 5
PLUGIN.OthersRankLevel = 5
PLUGIN.NeedTarget = false


PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	local Players = ents.FindByClass("gmod_playx")
	if #Players == 0 then
		local PlayXPlayer = ents.Create("gmod_playx")
		PlayXPlayer:SetModel("models/props/cs_office/projector.mdl")
		PlayXPlayer:SetPos(Vector(767.906250, 502.062500, 290.656250))
		PlayXPlayer:SetAngles(Angle(0, -180, 0))
		PlayXPlayer:Spawn()
		PlayXPlayer:Activate()
		PlayXPlayer:GetPhysicsObject():EnableMotion(false)
		if not Silent then
			if IsValid(Ply) then
				DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " Has spawned a PlayX Player")
			else
				DuckyAdmin.Message(false, Color(0, 150, 255), "Console ", Color(255, 255, 255), " Has spawned a PlayX Player")
			end
		end
	else	
		DuckyAdmin.Message(true, Ply, Color(255, 255, 255), " There is already a PlayX Player on the map")
	end
end

PLUGIN.Hooks["PlayXIsPermitted"] = function(Ply, PXPlayer) 
	return CheckRank(Ply, PLUGIN)
end

PLUGIN.Hooks["InitPostEntity"] = function()
	print("--------------Ran PlayX Hook!!!-----------------")
	local PlayXPlayer = ents.Create("gmod_playx")
	PlayXPlayer:SetModel("models/props/cs_office/projector.mdl")
	PlayXPlayer:SetPos(Vector(767.906250, 502.062500, 290.656250))
	PlayXPlayer:SetAngles(Angle(0, -180, 0))
	PlayXPlayer:Spawn()
	PlayXPlayer:Activate()
	print("Player:", PlayXPlayer)
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)
