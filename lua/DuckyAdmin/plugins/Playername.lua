local PLUGIN = {}
PLUGIN.Name = "Playername"
PLUGIN.Command = "Playername"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 1
PLUGIN.OthersRankLevel = 6
PLUGIN.NeedTarget = true

PLUGIN.RunFunction = function( Ply, Args, Targets, InConsole)
	if #Targets == 1 then
		local Target = Targets[1]
		local SteamID = Target:SteamID()
		local OldNick = Target:GetName()
		DuckyAdmin.SDB.Players[SteamID].UpdatedNick = true
		DuckyAdmin.SDB.Players[SteamID].Nickname = Args[1]
		if not Silent then
			DuckyAdmin.Message(false, Target:GetRankColor(), "[", Target:GetRankName(), "]", Color(60, 60, 60), OldNick, Color(255, 255, 255), " Is now know as ", Target)
		end
		net.Start("DuckyAdmin_UpdatePlayers")
		net.WriteTable(DuckyAdmin.SDB.Players)
		net.Broadcast()
	elseif #Targets > 1 then
		DuckyAdmin.MultiTarget(Ply)
	elseif #Targets == 0 then
		DuckyAdmin.NoTarget(Ply)
	end
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)
