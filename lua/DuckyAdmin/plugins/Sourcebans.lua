local PLUGIN = {}
PLUGIN.Name = "Sourcebans"
PLUGIN.Hooks = {}
PLUGIN.Hooks["player_connect"] = function(Data) 
	local IP = string.Explode(":", Data.address)[1]
	sourcebans.CheckForBan( Data.networkid, function() end)
end
PLUGIN.Hooks["DuckyAdmin_Ban"] = function(SteamID, Ply, Reason, UnbanTime, BannedByStemaID)
	sourcebans.BanPlayerBySteamID (SteamID, UnbanTime-os.time(), Reason, BannedByStemaID, "unknown", function() end)
end
PLUGIN.Hooks["DuckyAdmin_Unban"] = function(SteamID)
	sourcebans.UnbanPlayerBySteamID(SteamID, "Unbanned", nil)
end

require("sourcebans")
sourcebans.SetConfig("hostname", "127.0.0.1")
sourcebans.SetConfig("username", "root")
sourcebans.SetConfig("password", "")
sourcebans.SetConfig("database", "duckyadmin")
sourcebans.SetConfig("dbprefix", "sb")
sourcebans.SetConfig("portnumb", 3306)
sourcebans.SetConfig("serverid", 1)
sourcebans.SetConfig("website", "www.magnushj.dk/bans")
sourcebans.SetConfig("showbanreason", false)
sourcebans.SetConfig("dogroups", false)
sourcebans.Activate()


DuckyAdmin.PH.SubmitPlugin(PLUGIN)
