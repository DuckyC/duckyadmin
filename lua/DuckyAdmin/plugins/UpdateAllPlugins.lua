local PLUGIN = {}
PLUGIN.Name = "UpdateAllPlugins"
PLUGIN.Command = "UAP"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.SelfRankLevel = 10
PLUGIN.OthersRankLevel = 10
PLUGIN.NeedTarget = false

PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	for _, FileName in pairs(file.Find("DuckyAdmin/plugins/*.lua", "LUA")) do
		Msg("Loading "..FileName.."->")
		include("DuckyAdmin/plugins/"..FileName)
		Msg(" Loaded\n")
	end
	DuckyAdmin.Message(true, Color(60, 60, 60), "Updated all plugins")
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)