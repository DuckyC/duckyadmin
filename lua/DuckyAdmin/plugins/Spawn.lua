local PLUGIN = {}
PLUGIN.Name = "Spawn"
PLUGIN.Command = "Spawn"
PLUGIN.Prefix = "!"
PLUGIN.SilentPrefix = "@"
PLUGIN.OthersRankLevel = 6
PLUGIN.SelfRankLevel = 1
PLUGIN.NeedTarget = true

PLUGIN.RunFunction = function( Ply, Args, Targets, Silent)
	if #Targets > 0 then
		for _, TPly in pairs(Targets) do
			TPly:Spawn()
		end
		if not Silent then
			if IsValid(Ply) then
				DuckyAdmin.Message(false, Ply, Color(255, 255, 255), " Has Spawned ", unpack(Targets))
			else
				DuckyAdmin.Message(false, Color(0, 150, 255), "Console ", Color(255, 255, 255), " Has Spawned ", unpack(Targets))
			end
		end
	else
		DuckyAdmin.NoTarget(Ply)
	end
end

DuckyAdmin.PH.SubmitPlugin(PLUGIN)